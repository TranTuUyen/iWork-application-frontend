import { NgModule, group } from '@angular/core'

import { ApplicationRouting } from './application.routing'
import { WCommonModule } from '@W-Base/modules'
import { MainModule } from '@W-App/pages/main/main_config/main.module.ts'
import { TranslateModule } from 'ng2-translate'

import { SocketService } from '@W-Base/services'
import * as IkState from '@W-Base/states'

import { ApplicationComponent } from './application.component'
import { AccountModule } from '@W-App/pages/account/account_config/account.module';

import { Ng2DragDropModule } from 'ng2-drag-drop';

@NgModule({
	imports: [
		ApplicationRouting,
		WCommonModule,
		AccountModule,
		MainModule,
		TranslateModule,
		Ng2DragDropModule.forRoot()
		
	],
	providers: [
		IkState.MainState,
		IkState.CurrentUserState, 
		IkState.TeamState,
		SocketService
	],
	declarations: [
		ApplicationComponent
	]
})

export class ApplicationModule { }