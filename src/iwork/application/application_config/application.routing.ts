import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { ApplicationComponent } from './application.component'

const appRoutes: Routes = [
  {
    path: '',
    component: ApplicationComponent,
    children: [
      {
        path: '',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], (require: any) => {
            resolve(require('../pages/main/main_config/main.module').MainModule)
          })
        })
      },
      {
        path: 'account',
        loadChildren: () => new Promise(resolve => {
          (require as any).ensure([], (require: any) => {
            resolve(require('../pages/account/account_config/account.module').AccountModule)
          })
        })
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})

export class ApplicationRouting { }

