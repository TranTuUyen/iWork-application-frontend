import { Component, OnInit } from '@angular/core'
import { SocketService } from '@W-Base/services'

@Component({
	selector: 'application',
	templateUrl: './application.html',
	styleUrls: [
		'./../../../scss/page.scss',
		'./application.scss'
	]
})

export class ApplicationComponent implements OnInit {

	
	constructor(private socketService: SocketService) {
	}

	ngOnInit() {
		SocketService.socket.on('broadcast', function (data: any) { console.log(data) })
	}

}
