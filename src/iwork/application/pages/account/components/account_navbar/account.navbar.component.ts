import {Component} from '@angular/core'

@Component({
  selector: 'account-navbar',
  templateUrl: './account.navbar.html',
  styleUrls: [
    './../../../../../../scss/page.scss',
    './../../../../../../scss/ikicon.scss',
    './../../../../../../scss/elements.scss',
    './../../../../../../scss/button.scss',
    '../../../../../../scss/user.scss',
    './../../../../../../scss/helper.scss'
  ],
})

export class AccountNavbarComponent {
}