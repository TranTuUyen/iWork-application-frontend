import {Component} from '@angular/core'

@Component({
    selector: 'account-menu',
    templateUrl: './account.menu.html',
    styleUrls: [
        '../../../../../../scss/page.scss',
        '../../../../../../scss/form.scss',
        '../../../../../../scss/button.scss',
        '../../../../../../scss/elements.scss',
        './account.menu.scss',
        '../../../../../../scss/helper.scss'
    ]
})

export class AccountMenuComponent {
    currentItem = 3

    setCurrentItem(newCurrentItem: number) {
        this.currentItem = newCurrentItem
    }
}