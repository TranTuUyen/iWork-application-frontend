import {Injectable} from '@angular/core'
import {Http, RequestOptions} from '@angular/http'
import {Observable} from 'rxjs/Observable'
import { AuthHttpService } from '@W-Base/services'
import { AccountAPIs } from '@W-Base/apis'

@Injectable()
export class AccountService {
    constructor(private authHttp: AuthHttpService) {}

    public getAccountProfile(): Observable<any> {
        return this.authHttp.get(AccountAPIs.ACCOUNT_INFO)
            .map(response => response._body)
    }

    public changePassword(data: any): Observable<any> {
        return this.authHttp.put(AccountAPIs.CHANGE_PASS, data)
            .map(response => response)
            .catch(error => Observable.throw(error || 'Server error'))
    }

    public updateAccountProfile(data: any): Observable<any> {
        console.log('Push data', data)
        
        return this.authHttp.put(AccountAPIs.UPDATE_PROFILE, data)
            .map(response => response)
            .catch(error => Observable.throw(error || 'Server error'))
    }
}