import { Component, ViewChild, OnInit } from '@angular/core'
import { Observable } from 'rxjs/Observable'

import { AuthHttpService } from '@W-Base/services'
import { AccountService } from '../../services/account.service'
import { CommonVariables } from '@W-Base/variables'
import { ToasterService } from '@W-App/plugin/toaster/toaster.service';
import { IToasterConfig, ToasterConfig } from '@W-App/plugin/toaster/toaster-config';

@Component({
	selector: 'profile_settings',
	templateUrl: './profile.settings.html',
	styleUrls: [
		'../../../../../../scss/page.scss',
		'../../../../../../scss/elements.scss',
		'../../../../../../scss/section.scss',
		'../../../../../../scss/form.scss',
		'../../../../../../scss/button.scss',
		'../../../../application_config/application.scss',
		'../../account_config/account.scss',
		'./profile.settings.scss',
		'../../../../../../scss/helper.scss']
})

export class ProfileSettingsComponent implements OnInit {
	defaultProfilePicture = 'assets/images/account/avt_224x224.png';
	preloading = "assets/images/circle_loading.gif";
	accountData = {
		id: '',
		avatar: '',
		email: '',
		userInfo: {
			firstname: '',
			profilePicture: '',
		}
	}

	fieldError = {
		firstname: false,
	}

	errorMessage = {
		firstname: '',
	}

	private toasterService: ToasterService;
	private appToasterConfig: IToasterConfig = new ToasterConfig({
			animation: 'fade',
			newestOnTop: false,
			limit: 5,
			defaultTypeClass: 'toast-top-right'
	});
	constructor(private accountService: AccountService,
		private authHttp: AuthHttpService,
		private _toasterService: ToasterService) {
			this.toasterService = _toasterService;
	}

	ngOnInit(): void {
		this.accountService.getAccountProfile()
			.subscribe(
			response => { 
				let result = JSON.parse(response)
				this.accountData = result.data 
			},
			error => {
				let errorResponse = JSON.parse(error._body)
				var toast: any = {
						type: "danger",
						body: errorResponse.message
				};
				
				this.toasterService.pop(toast);
			})
	}

	previewImage(event: any) {
		
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader()

			reader.onload = (data) => {
				//TODO: this.accountData.userInfo.profilePicture = event.target.result
				this.defaultProfilePicture = event.target.result
			}

			reader.readAsDataURL(event.target.files[0])
		}
	}
	loadingavatar: any;

	fileChange(event: any): void {
		this.loadingavatar = true
		const files: FileList = event.target.files;
		if (files.length <= 0) {
			return
		}
		// this.fileService.uploadFile(files[0])
		// 	.subscribe(response => {
		// 		this.accountData.userInfo.profilePicture = `${FileAPIs.FILE_BASE_URL}/${response.filename}`
		// 		this.loadingavatar = false
		// 	}, error => {
		// 		//TODO: need implement later
		// 		alert(error)
		// 		console.log(error)
		// 	})
	}

	removeAvata() {
		this.accountData.userInfo.profilePicture = this.defaultProfilePicture;
	}


	public onSaveProfile() {

		var new_data = {
			"userInfo": {
				"firstname": this.accountData.userInfo.firstname,
				"profilePicture":  this.accountData.userInfo.profilePicture,
			}
		}
		
		if (!this.isValid()) {
			return
		}
		this.accountService.updateAccountProfile(new_data)
			.subscribe(
			response => {
				var data = JSON.parse(response._body); 
 
				var toast: any = {
						type: "success",
						body: data.message
				}; 
				this.toasterService.pop(toast);

			},
			error => {
			  let errorResponse = JSON.parse(error._body)
				var toast: any = {
						type: "danger",
						body: errorResponse.message
				};
				
				this.toasterService.pop(toast);
			})
	}

	public resetProfilePicture() {
		// TODO: this.accountData.userInfo.profilePicture = this.defaultProfilePicture
	}

	isValid(): boolean {
		this.validateFirstName()
		for (let field in this.fieldError) {
			if (this.fieldError[field] === true) {
				return
			}
		}

		return true
	}

	validateFirstName() {
		this.fieldError.firstname = false
		if (!this.accountData.userInfo.firstname || this.accountData.userInfo.firstname === '') {
			this.fieldError.firstname = true
			this.errorMessage.firstname = CommonVariables.FIRST_NAME_REQUIRED

			return
		} else if (this.accountData.userInfo.firstname.length > CommonVariables.FIELD_NAME_MAX_LENGTH) {
			this.fieldError.firstname = true
			this.errorMessage.firstname = CommonVariables.FIELD_NAME_LENGTH

			return
		}
	}

	onKeyPress(field: any) {
		this.fieldError[field] = false
	}
}