import { Component, OnInit, ViewChild } from '@angular/core'
import { AccountService } from '../../services/account.service'
import { CommonVariables } from '@W-Base/variables'
import { ToasterService } from '@W-App/plugin/toaster/toaster.service';
import { IToasterConfig, ToasterConfig } from '@W-App/plugin/toaster/toaster-config';

@Component({
    selector: 'change_password',
    templateUrl: './change.password.html',
    styleUrls: [
        '../../../../../../scss/page.scss',
        '../../../../../../scss/elements.scss',
        '../../../../../../scss/section.scss',
        '../../../../../../scss/form.scss',
        '../../../../../../scss/button.scss',
        '../../../../application_config/application.scss',
        '../../account_config/account.scss',
        'change.password.scss',
        '../../../../../../scss/helper.scss']
})

export class ChangePasswordComponent {
   
    data = {
        currentPassword: '',
        newPassword: ''
    }
    confirmPassword = ''

    fieldErrors = {
        currentPassword: false,
        newPassword: false,
        confirmPassword: false
    }

    validationMessages = {
        currentPassword: '',
        newPassword: '',
        confirmPassword: ''
    }
    sttChangeSuccess: boolean;
    
	private toasterService: ToasterService;
	private appToasterConfig: IToasterConfig = new ToasterConfig({
			animation: 'fade',
			newestOnTop: false,
			limit: 5,
			defaultTypeClass: 'toast-top-right'
	});
    constructor(private accountService: AccountService,private _toasterService: ToasterService) {
        this.toasterService = _toasterService;
}

     

    validateCurrentPassword() {
        this.fieldErrors.currentPassword = false;

        if (this.data.currentPassword.length === 0) {
            this.fieldErrors.currentPassword = true
            this.validationMessages.currentPassword = CommonVariables.CURRENT_PASSWORD_REQUIRED

            return
        }
        if (this.data.currentPassword.length < 8 || this.data.currentPassword.length > 30) {
            this.fieldErrors.currentPassword = true
            this.validationMessages.currentPassword = CommonVariables.CURRENT_PASSWORD_REQUIRED_LENGTH

            return
        }
    }

    validateNewPassword() {
        this.fieldErrors.newPassword = false;

        if (this.data.newPassword.length === 0) {
            this.fieldErrors.newPassword = true
            this.validationMessages.newPassword = CommonVariables.NEW_PASSWORD_REQUIRED

            return
        }
        if (this.data.newPassword.length < 8 || this.data.newPassword.length > 30) {
            this.fieldErrors.newPassword = true
            this.validationMessages.newPassword = CommonVariables.NEW_PASSWORD_REQUIRED_LENGTH

            return
        }
    }

    validateConfirmPassword() {
        this.fieldErrors.confirmPassword = false
        if (this.confirmPassword.length > 0 && this.confirmPassword !== this.data.newPassword) {
            this.fieldErrors.confirmPassword = true
            this.validationMessages.confirmPassword = CommonVariables.ALERT_CONFIRM_PASSWORD_NOT_MATCH

            return
        }
    }
    public changePassword() {
        this.clearInvalidFields()
        if (this.isValid()) {
            console.log('Everything is OK', this.data)
            this.accountService.changePassword(this.data)
                .subscribe(
                response => {
                    let res = JSON.parse(response._body)
                    console.log(res)
                    this.sttChangeSuccess = true;
                        this.data = {
                            currentPassword: '',
                            newPassword: ''
                        };
                        this.confirmPassword = '';
                        var toast: any = {
                            type: "success",
                            body: res.message
                    }; 
                    this.toasterService.pop(toast);
                },
                error => {
                    let res = JSON.parse(error._body)
                    console.log('Have an error', res)
                    if (res.statusCode === 400 || res.message === 'Current password does not match') {
                        this.fieldErrors.currentPassword = true
                        this.validationMessages.currentPassword = CommonVariables.ALERT_CURRENT_PASSWORD_NOT_MATCH
                    }
                }
                )
        } else {
            return
        }
    }

    onKeyDown(field: any) {
        this.fieldErrors[field] = false
    }

    private isValid(): boolean {
        this.validateCurrentPassword();
        this.validateNewPassword();
        this.validateConfirmPassword()
        for (let field in this.fieldErrors) {
            if (this.fieldErrors[field] === true) {
                return false
            }
        }

        return true
    }

    private clearInvalidFields() {
        for (const field in this.fieldErrors) {
            this.fieldErrors[field] = false
        }
    }
}