import {Component} from '@angular/core'
import {ViewEncapsulation} from '@angular/core'

@Component({
    selector: 'account',
    templateUrl: './account.html',
    styleUrls: [
        './../../../../../scss/page.scss',
        './../../../../../scss/ikicon.scss',
        './../../../../../scss/elements.scss',
        './account.scss',
        './../../../../../scss/helper.scss'
    ],
    encapsulation: ViewEncapsulation.None
})

export class AccountComponent {
}