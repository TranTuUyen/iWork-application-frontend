export class AccountVariables {

    public static API_ACCOUNT_INFO = 'http://localhost:8080/account/current'
    public static API_CHANGE_PASS = 'http://localhost:8080/account/change-password'
    public static API_UPDATE_PROFILE = 'http://localhost:8080/account/edit-account'

}