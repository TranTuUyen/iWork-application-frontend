import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {AccountComponent} from './account.component'
import {
  ChangePasswordComponent,
  ProfileSettingsComponent,
} from '@W-AppPages/account/pages'

const accountRoutes: Routes = [
  {
    path: '',
    component: AccountComponent,

    children: [
      {
        path: 'change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'profile-setting',
        component: ProfileSettingsComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(accountRoutes)],
  exports: [RouterModule]
})

export class AccountRouting {
}
