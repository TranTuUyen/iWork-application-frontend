import {NgModule} from '@angular/core'
import {AccountRouting} from './account.routing'

import {AccountComponent} from './account.component'
import {TranslateModule} from 'ng2-translate'
import {AccountMenuComponent, AccountNavbarComponent} from '@W-AppPages/account/components'
import {ProfileSettingsComponent} from '@W-AppPages/account/pages/profile_settings/profile.settings.component'
import {ChangePasswordComponent} from '@W-AppPages/account/pages/change_password/change.password.component'
import {MessagePopupComponent} from '@W-Base/components'
import {Angular2FontawesomeModule} from 'angular2-fontawesome/angular2-fontawesome'
import {JWTService} from '@W-Base/services'
import {AccountService} from '@W-AppPages/account/services/account.service'
import {FormsModule} from '@angular/forms'
import {CommonModule} from '@angular/common'
import { OnlyNumber } from '@W-Base/directions/onlynumber.direction';
import { ClickOutside } from '@W-Base/directions';

import { ToasterModule } from '@W-App/plugin/toaster/toaster.module';

@NgModule({
  imports: [
    AccountRouting,
    TranslateModule,
    Angular2FontawesomeModule,
    FormsModule,
    CommonModule,
    ToasterModule
  ],
  providers: [
    JWTService,
    AccountService,
  ],
  declarations: [
    AccountComponent,
    AccountMenuComponent,
    AccountNavbarComponent,
    ProfileSettingsComponent,
    ChangePasswordComponent,
    MessagePopupComponent,

    OnlyNumber,
    ClickOutside
  ]
})

export class AccountModule {
}