import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'
import { Http } from '@angular/http';
import { TeamAPIs, CardApis } from '@W-Base/apis';
import * as io from 'socket.io-client';
import { AuthHttpService, SocketService } from '@W-Base/services';

@Injectable()
export class CardHttpService {
  // private socket: SocketIOClient.Socket;
  constructor(private http: Http, private authHttpService: AuthHttpService) {
    // this.socket = io('http://localhost:3000');
  }

  getCardInfo(cardId: string) : Observable<any>{
    return this.authHttpService.get(CardApis.GET_CARD_INFO + `${cardId}`)
    .map(res => {
      return res.json()
    })
  }

  createNewCard(card: any) {
    // this.socket.emit('createNewCard', card);
    console.log(card);
    return  this.authHttpService.post(CardApis.GET_CARD, card)
    .map(response => {
      console.log(response)})
    .catch(error => Observable.throw(error || 'Server error'))
  }

  onNewCard() {
    return Observable.create((observer: any) => {
      SocketService.socket.on('new-card', (card: any) => {
        observer.next(card);
      });
    });
  }

  editCard(card: ICard) {
    //this.socket.emit('editCard',card);
    return  this.authHttpService.put(CardApis.GET_CARD+`${card._id}`, card)
    .map(response => response)
    .catch(error => Observable.throw(error || 'Server error'))
  }

  onEditCard() {
    return Observable.create((observer: any) => {
      SocketService.socket.on('update-card', (card: any) => {
        observer.next(card);
      });
    });
  }

  removeCard(card: ICard, teamId: string) {
    // this.socket.emit('removeCard',teamId,card);
    let data: any = {
      _id: teamId
    }
    return  this.authHttpService.delete(CardApis.GET_CARD+`${card._id}`, data)
    .map(response => {
      console.log(response)})
    .catch(error => Observable.throw(error || 'Server error'))
  }

  onRemoveCard() {
    return Observable.create((observer: any) => {
      SocketService.socket.on('delete-card', (card: any) => {
        observer.next(card);
        console.log("Card deleted: ");
        console.log(card);
      });
    });
  }


  public getCardsByTeam(teamId: string): Observable<ICard[]> {
    return this.authHttpService.get(CardApis.GET_CARD + `${teamId}`)
      .map(response => {
        return response.json()
      })
  }

  public updateCardMember(cardId: String, action: String, memberId: string, teamId: string) {
    let data: any = {
      _id: teamId
    }
    return this.authHttpService.put(CardApis.GET_CARD + `${cardId}` + `/` + `${action}` + `/` + `${memberId} `,data)
    .map(response => response.json())
      .catch(error => Observable.throw(error || 'Server error'))
  }

  public onUpdateCardMember() {
    return Observable.create((observer: any) => {
      SocketService.socket.on('update-member-card', (card: any) => {
        observer.next(card);
      });
    });
  }

  public updateCard(cardData: ICard): Observable<ICard> {
    return this.authHttpService.put(CardApis.GET_CARD + `${cardData._id}`, cardData)
      .map(response => response)
      .catch(error => Observable.throw(error || 'Server error'))
  }

  // Activity
  onNewActivity() {
    return Observable.create((observer: any) => {
      SocketService.socket.on('new-activity', (activity: any) => {
        observer.next(activity);
      });
    });
  }
}