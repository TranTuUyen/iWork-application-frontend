import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'
import { Http ,Headers} from '@angular/http';
import { Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { TeamAPIs } from '@W-Base/apis';
import { TeamState } from '@W-Base/states';
import * as io from 'socket.io-client';
import { JWTService, AuthHttpService, SocketService } from '@W-Base/services';
import { MemberAPIs } from '@W-Base/apis/member.api';


@Injectable()
export class TeamHttpService {
    constructor(private http: Http,private jwtService: JWTService, private authHttpService: AuthHttpService) {
    }

    public getJoinedTeams(): Observable<any> {
        return this.authHttpService.get(TeamAPIs.JOINED_TEAMS)
            .map(response => {
                return response.json();
            })
    }

    public getMemberInfo(userId: String): Observable<any> {
        return this.authHttpService.get(MemberAPIs.GET_MEMBER+ `${userId}`)
            .map(response => {
                return response.json();
            })
    }

    public getMembersInfo(userIds: String[]): Observable<any> {
        return this.authHttpService.get(MemberAPIs.GET_MEMBERS, userIds)
            .map(response => {
                return response.json();
            })
    }

    public createNewTeam(team: any) {
               
        return this.authHttpService.post(TeamAPIs.CREATE_TEAM, team)
            .map(response => response)
            .catch(error => Observable.throw(error || 'Server error'))
    }

    // public onNewTeam() {
    //     return Observable.create((observer: any) => {
    //         this.socket.on('newTeam', (team: any) => {
    //             observer.next(team);
    //         });
    //     });
    // }

    public joinTeamRoom(teamId: string) {
        SocketService.socket.emit("join-team-room", teamId);
    }

    public removeTeam(teamId: string) {
        // this.socket.emit('removeTeam', teamId);
        return this.authHttpService.delete(TeamAPIs.CREATE_TEAM + `${teamId}`)
            .map(response => response)
            .catch(error => Observable.throw(error || 'Server error'))
    }

    // public onRemoveTeam() {
    //     return Observable.create((observer: any) => {
    //         this.socket.on('removeTeam', (teamId: string) => {
    //             observer.next(teamId);
    //         });
    //     });
    // }

    // Update
    public updateTeam(team: ITeam) {
        return this.authHttpService.put(TeamAPIs.CREATE_TEAM + `${team._id}`, team)
            .map(response => {
                return response.json()
            })
    }

    public updateTeamMember(teamId: String, action: String, memberId: String, data: any) {
        return this.authHttpService.put(TeamAPIs.CREATE_TEAM + `${teamId}` +`/`+ `${action}`+`/` + `${memberId}`, data)
        .map(response => {
            return response.json()
        })
    }

    //Member
    getMemberByEmail(email: string) {
        return this.authHttpService.get(MemberAPIs.GET_MEMBER_BY_EMAIL+ `${email}`)
            .map(response => {
                return response.json();
            })
    }

}