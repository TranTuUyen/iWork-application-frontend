import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'
import { Http } from '@angular/http';
import { TeamAPIs, CardApis } from '@W-Base/apis';
import * as io from 'socket.io-client';
import { AuthHttpService, SocketService } from '@W-Base/services';
import { ActivityApis } from '@W-Base/apis/activity.api';

@Injectable()
export class ActivityHttpService {

    constructor(private http: Http, private authHttpService: AuthHttpService) {
    }

    getActivitiesByTeam(teamId: string) : Observable<any[]>{
        return this.authHttpService.get(ActivityApis.GET_ACTIVITY +  `${teamId}`)
        .map(response => {
            return response.json()
          })
    }
}