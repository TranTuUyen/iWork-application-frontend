import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { MainComp } from './main.comp'

const chatRoutes: Routes = [
	{
		path: '',
		component: MainComp
	}
]

@NgModule({
	imports: [RouterModule.forChild(chatRoutes)],
	exports: [RouterModule]
})

export class MainRouting { }

