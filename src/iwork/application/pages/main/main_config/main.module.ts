import { NgModule } from '@angular/core'
import { TagInputModule } from 'ngx-chips'
import { TranslateModule } from 'ng2-translate'
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'
import { WCommonModule } from '@W-Base/modules'
import { MainRouting } from './main.routing'

import { MainComp } from './main.comp'
import * as MainComps from '../components'
import { MainHttpService } from '@W-App/pages/main/services/main.service'; 
import { AvatarModule } from 'ngx-avatar';
import { ToasterModule } from '@W-App/plugin/toaster/toaster.module';
// import {DynamicIcon} from "@W-Base/directions/dynamicIcon.direction";

import {DragulaModule} from "ng2-dragula";
import { ClickOutsideModule } from 'ng4-click-outside';
import { CardHttpService } from '@W-App/pages/main/services/card.service';
@NgModule({
  imports: [
    TagInputModule,
    MainRouting,
    TranslateModule,
    Angular2FontawesomeModule,
    WCommonModule,
    AvatarModule,
    ToasterModule,
    DragulaModule,
    ClickOutsideModule
    
  ],
	providers: [
    MainHttpService
  ],
  declarations: [
    MainComp,
    MainComps.TodoSidebarComp,
    MainComps.TodoContentComp,
    MainComps.TaskComp,
    MainComps.TaskDetailComp,
    MainComps.TeamComp,
    MainComps.TeamInvitationModalComp,

    MainComps.BoardDetailComp,
    MainComps.ViewCardModalComp,
    MainComps.BoardMenuComp,

    MainComps.LeftMenuComponent,
    MainComps.UserMenuComp,


  ],
  exports: [
    MainComp
  ]
})

export class MainModule { }