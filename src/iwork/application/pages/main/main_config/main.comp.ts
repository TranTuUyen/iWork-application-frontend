import { Component, ViewEncapsulation } from '@angular/core'
import { Subscription } from 'rxjs'
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { MainState } from '@W-Base/states'
import { ThreadType } from '@W-Base/enums'
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
	selector: 'main',
	templateUrl: './main.html',
	styleUrls: [
		'../../../../../scss/page.scss',
		'../../../../../scss/section.scss',
		'../../../../../scss/form.scss',
		'../../../../../scss/button.scss',
		'../../../../../scss/elements.scss',
		'../../../../../scss/modal.scss',
		'../../../../../scss/right_panel.scss',
		'../../../../../scss/custom.scss',
		'./main.scss',
		'../../../../../scss/helper.scss'],
	encapsulation: ViewEncapsulation.None

})

export class MainComp implements OnInit {

	mainStateSub: Subscription

	mainContentState: any
	constructor(private mainState: MainState, private http: Http) {
		//TODO: Should load from local storage
		this.mainState.switchToBoard();
	}
	ngOnInit() {
		this.mainState.currentMainState.subscribe(mainContentState => {
		this.mainContentState = mainContentState
			console.log(mainContentState)
		})
	}

}