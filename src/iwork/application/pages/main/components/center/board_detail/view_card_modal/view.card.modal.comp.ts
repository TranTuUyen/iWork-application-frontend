import { Component, OnInit, Input, Output, ChangeDetectorRef } from '@angular/core'
import { TeamState } from '@W-Base/states';
import { EventEmitter } from '@angular/core';
import { CardHttpService } from '@W-App/pages/main/services/card.service';
import { TeamHttpService } from '@W-App/pages/main/services/team.service';
import { JWTService } from '@W-Base/services';

@Component({
  selector: 'view-card-modal',
  templateUrl: './view.card.modal.html',
  styleUrls: [
    './view.card.modal.scss'
  ]
})

export class ViewCardModalComp implements OnInit {
  teamAvt = 'assets/images/smile_avt.jpg'
  toggleInvite: boolean = false;
  currentCard: ICard;
  currentTeam: ITeam;
  teamMemberArray: any = [];
  cardMemberArray: any = [];
  memberNotInCard: any = [];
  userInfo = {} as IUser;
  isMember: boolean = false;
  isAdmin: boolean = false;

  @Input()
  inputToggle: boolean = false;
  @Output() closeModal = new EventEmitter();
  constructor(private teamState: TeamState,
    private teamCardService: CardHttpService,
    private teamHttpService: TeamHttpService,
    private jwtService: JWTService,
    private cdr: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.userInfo = JSON.parse(this.jwtService.getUserInfo())
    this.teamCardService.onUpdateCardMember()
      .subscribe((member: any) => {
        if (!this.checkCardMemberExist(member)) {

          this.currentCard.members.push(member._id);
        }
        else {
          const index1: number = this.currentCard.members.map((e: any) => e).indexOf(member._id);
          if (index1 !== -1) {
            this.currentCard.members.splice(index1, 1);
          }
        }
        this.getCardMembers();
        this.filterMember();

      })
    this.teamState.getCurrentTeam()
      .subscribe(res => {
        this.currentTeam = res;
        console.log(res);
        this.getCurrentTeamMembers();

      },
        error => {
          alert('error')
          console.log(error)
        })

    this.teamState.getCurrentCard()
      .subscribe(res => {
        this.currentCard = res;
        this.getCardMembers();
        this.filterMember();
      },
        error => {
          console.log(error)
        })

  }

  //Get curent team members
  getCurrentTeamMembers() {
    for (let i = 0; i < this.currentTeam.members.length; i++) {
      this.teamHttpService.getMemberInfo(this.currentTeam.members[i].member)
        .subscribe(res => {
          let member = res;
          if (this.currentTeam.members[i]) {
            this.teamMemberArray.push(member);
          }
        })
      this.checkAdmin();
    }
  }

  getCardMembers() {
    this.cardMemberArray = [];
    for (let i = 0; i < this.currentCard.members.length; i++) {
      this.teamHttpService.getMemberInfo(this.currentCard.members[i])
        .subscribe(res => {
          let member = res;
          if (this.currentTeam.members[i]) {
            this.cardMemberArray.push(member);
            this.checkMember(member);
            this.cdr.detectChanges()
          }
        })
    }
    console.log(this.cardMemberArray);
  }

  comparer(otherArray: any[]) {
    return function (current: any) {
      return otherArray.filter(function (other) {
        return other.member == current || other == current.member;
      }).length == 0;
    }
  }

  checkCardMemberExist(member: any): boolean {
    return this.currentCard.members.some(function (el: any) {
      return el == member._id;
    });
  }

  filterMember() {
    let a: any[] = this.currentTeam.members;
    let b: any[] = this.currentCard.members;

    var onlyInA = a.filter(this.comparer(b));
    var onlyInB = b.filter(this.comparer(a));

    let result = onlyInA.concat(onlyInB);
    console.log("Result")
    console.log(result);
    this.memberNotInCard = [];
    for (let i = 0; i < result.length; i++) {
      this.teamHttpService.getMemberInfo(result[i].member)
        .subscribe(res => {
          let member = res;
          this.memberNotInCard.push(res);
          this.cdr.detectChanges()
        })
    }
  }

  updateCardMember(action: String, memberId: string) {
    this.teamCardService.updateCardMember(this.currentCard._id, action, memberId, this.currentTeam._id)
      .subscribe(res => {
        this.toggleInvite = false;
        if (action == "add-member") {
          // this.checkMember();
          // this.checkAdmin();
        }
        this.cdr.detectChanges()
      },
        err => {
          console.log(err)
        })
  }

  updateCard(card: ICard) {
    this.close();
    this.teamCardService.updateCard(card)
      .subscribe(res => {
        console.log(res);
        this.close();
      },
        err => {
          console.log(err)
        })
  }

  checkMember(member: any) {
    console.log(member)
    if (this.userInfo._id == member._id) {
      this.isMember = true;
    }
  }

  checkAdmin() {
    for (let i = 0; i < this.currentTeam.members.length; i++) {
      if (this.currentTeam.members[i].member == this.userInfo._id && this.currentTeam.members[i].permission == 'Admin') {
        this.isAdmin = true;
      }
    }
  }

  close() {
    this.closeModal.emit();
  }

}