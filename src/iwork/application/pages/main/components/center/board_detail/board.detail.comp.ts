import { Component, OnInit, ChangeDetectorRef  } from '@angular/core'
import { DragulaService } from 'ng2-dragula';
import { Http } from '@angular/http';
import { TeamState, MainState } from '@W-Base/states';
import { Observable } from 'rxjs/Observable';
import { LeftMenuComponent } from '@W-App/pages/main/components';
import { TeamAPIs } from '@W-Base/apis';
import { TeamHttpService } from '@W-App/pages/main/services/team.service';
import * as io from 'socket.io-client';
import { CardHttpService } from '@W-App/pages/main/services/card.service';


@Component({
  providers: [LeftMenuComponent],
  selector: 'board-detail',
  templateUrl: './board.detail.html',
  styleUrls: [
    './board.detail.scss'
  ]
})

export class BoardDetailComp implements OnInit {
  teamAvt = 'assets/images/smile_avt.jpg';
  showedMenu: number = 0;
  teamName: String;
  openMenu: boolean = false;
  toggleNewTeam: boolean = false;
  focusNewCard1: boolean = false;
  focusNewCard2: boolean = false;
  focusNewCard3: boolean = false;
  focusNewCard4: boolean = false;
  currentTeam: ITeam;
  viewCardModal: boolean = false;

  newTeam = {
    name: ''
  };
  newCard = {
    content: '',
    status: '',
    index: 0,
    team: ''
  };


  cardsArr: ICard[];
  backLogCardsArr: ICard[] = [];
  todoCardsArr: ICard[] = [];
  doingCardsArr: ICard[] = [];
  doneCardsArr: ICard[] = [];

  constructor(private dragulaService: DragulaService,
    private http: Http,
    private mainState: MainState,
    private teamState: TeamState,
    private teamHttpService: TeamHttpService,
    private cardHttpService: CardHttpService,
    private comp: LeftMenuComponent,
    private cdr:ChangeDetectorRef) {
    dragulaService.setOptions('pineline-parent-1', {
      moves: function (el: Element, container: Element, handle: Element) {
        return handle.className === 'icon-cursor-move';
      }
    });

    this.dragulaService.drop.subscribe((args: any) => {
      const [bagName, elSource, bagTarget, bagSource, elTarget] = args;
      const newIndex = (elTarget ? this.getElementIndex(elTarget) : bagTarget.childElementCount) - 1;
      console.log(newIndex);

      console.log("bagTarget");
      console.log(bagTarget.id);

      console.log("bagSource");
      console.log(bagSource.id);

      let editingCard = {
        _id: elSource.id,
        index: newIndex,
        status: ''
      }

      if (bagTarget.id === 'backlogPineline') editingCard.status = 'backlog';
      if (bagTarget.id === 'todoPineline') editingCard.status = 'todo';
      if (bagTarget.id === 'doingPineline') editingCard.status = 'doing';
      if (bagTarget.id === 'donePineline') editingCard.status = 'done';

      this.editCard(editingCard);
    })
  }


  private getElementIndex(el: HTMLElement): number {
    return [].slice.call(el.parentElement.children).indexOf(el);
  }

  ngOnInit() {
    this.cardHttpService.onNewCard()
      .subscribe((card: ICard) => {
        if(card.team == this.currentTeam._id) {
          if (card.status === 'backlog') {
            console.log("backlog");
            this.backLogCardsArr.push(card);
            console.log(this.backLogCardsArr);
          }
          if (card.status === 'todo') {
            this.todoCardsArr.push(card);
          }
          if (card.status === 'doing') {
            this.doingCardsArr.push(card);
          }
          if (card.status === 'done') {
            this.doneCardsArr.push(card);
          }
        }
        
        this.cdr.detectChanges()
      })


    // this.teamHttpService.onNewTeam()
    //   .subscribe((team: ITeam) => {
    //     this.teamState.addNewTeam(team);
    //     this.teamState.setCurrentTeam(team);
    //   })

    this.cardHttpService.onEditCard()
      .subscribe((card: ICard) => {
        this.removeCardInSmallArr(card._id, this.backLogCardsArr);
        this.removeCardInSmallArr(card._id, this.todoCardsArr);
        this.removeCardInSmallArr(card._id, this.doingCardsArr);
        this.removeCardInSmallArr(card._id, this.doneCardsArr);
        this.addCardToSmallArr(card);
        this.cdr.detectChanges()        
      })

    this.cardHttpService.onRemoveCard()
      .subscribe((card: ICard) => {
        // if (card.status === 'backlog') {
          this.removeCardInSmallArr(card._id, this.backLogCardsArr);
        // }
        // if (card.status === 'todo') {
          this.removeCardInSmallArr(card._id, this.todoCardsArr);
        // }
        // if (card.status === 'doing') {
          this.removeCardInSmallArr(card._id, this.doingCardsArr);
        // }
        // if (card.status === 'done') {
          this.removeCardInSmallArr(card._id, this.doneCardsArr);
        // }
        this.cdr.detectChanges()        
      })

    const currentTeamReq = this.teamState.getCurrentTeam()
      .subscribe(res => {
        this.currentTeam = res;
        if(this.currentTeam.name)
          this.teamName = this.currentTeam.name;
        console.log("Current team");
        console.log(res);
      },
        error => {
          alert('error')
          console.log(error)
        })
    this.loadData();
  }

  removeCardInSmallArr(cardId: string, arr: any[]) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i]._id === cardId) {
        arr.splice(i, 1);
      }
    }
  }

  addCardToSmallArr(card: ICard) {
    if (card.status === 'backlog') {
      this.backLogCardsArr.push(card);
      this.backLogCardsArr.sort((a: ICard, b: ICard) => a.index.toString().localeCompare(b.index.toString()))
    }
    if (card.status === 'todo') {
      this.todoCardsArr.push(card);
      this.todoCardsArr.sort((a: ICard, b: ICard) => a.index.toString().localeCompare(b.index.toString()))
    }
    if (card.status === 'doing') {
      this.doingCardsArr.push(card);
      this.doingCardsArr.sort((a: ICard, b: ICard) => a.index.toString().localeCompare(b.index.toString()))
    }
    if (card.status === 'done') {
      this.doneCardsArr.push(card);
      this.doingCardsArr.sort((a: ICard, b: ICard) => a.index.toString().localeCompare(b.index.toString()))
    }
  }

  loadData() {
    this.teamState.getCardList()
      .subscribe(
        res => {
          this.cardsArr = res;
          this.backLogCardsArr = [];
          this.todoCardsArr = [];
          this.doingCardsArr = [];
          this.doneCardsArr = [];
          console.log(this.cardsArr);
          let len = this.cardsArr.length;
          for (let i = 0; i < len; i++) {
            const card = this.cardsArr[i];
            console.log(card.status);
            switch (card.status) {
              case ('backlog'):
                this.backLogCardsArr.push(card);
                break;
              case ('todo'):
                this.todoCardsArr.push(card);
                break;
              case ('doing'):
                this.doingCardsArr.push(card);
                break;
              case ('done'):
                this.doneCardsArr.push(card);
                break;
            }
          }
        },
        err => {
          console.log("Error occured");
        }
      );
  }

  ngOnDestroy() {
    this.dragulaService.destroy('pineline-parent-1');
  }

  onClickedOutside(e: Event) {
    this.toggleNewTeam = false;
  }

  toggleMenu() {
    this.openMenu = !this.openMenu;
    if (this.showedMenu !== 1) {
      this.showedMenu = 1;
    }
    else {
      this.showedMenu = 2;
    }
  }

  createTeam(team: any) {
    if (this.newTeam.name) {
      this.closeAllNewCard();
      const req = this.teamHttpService.createNewTeam(team)
      .subscribe((res: any) =>{
        console.log(res.json());
        this.teamState.addNewTeam(res.json());
      })
      this.toggleNewTeam = false;
    }
    else alert("Tên team rỗng")
  }


  createCard(data: any) {
    if (this.newCard.content) {
      if (this.newCard.status === 'backlog') {
        let temp = this.backLogCardsArr.length;
        this.newCard.index = temp;
      }
      
      if (this.newCard.status === 'todo') {
        let temp = this.todoCardsArr.length;
        this.newCard.index = temp;
      }
      
      if (this.newCard.status === 'doing') {
        let temp = this.doingCardsArr.length;
        this.newCard.index = temp;
      }
      
      if (this.newCard.status === 'done') {
        let temp = this.doneCardsArr.length;
        this.newCard.index = temp;
      }
      this.newCard.team = this.currentTeam._id;
      this.closeAllNewCard();
      const req = this.cardHttpService.createNewCard(this.newCard)
      .subscribe(() => {
        console.log("new card");
      })
    }
    else alert("Nội dung card rỗng")
  }

  editCard(data: any) {
    this.closeAllNewCard();
    console.log(data);
    const req = this.cardHttpService.editCard(data)
    .subscribe(res => {
      console.log(res);
      this.cdr.detectChanges()
      
    },
      err => {
        console.log(err)
      })
  }

  removeCard(card: ICard) {
    const req = this.cardHttpService.removeCard(card, this.currentTeam._id)
    .subscribe(() => {
      this.cdr.detectChanges()
      console.log("Delete  card successfully!");
    })
  }


  cancelCard() {
    this.closeAllNewCard();
    this.newCard.content = '';
  }

  closeAllNewCard() {
    this.focusNewCard1 = false;
    this.focusNewCard2 = false;
    this.focusNewCard3 = false;
    this.focusNewCard4 = false;
  }
  chooseCard = (card: ICard) => {
    this.cardsArr.forEach(card => card.current = false)
    card.current = true;
    this.viewCardModal = true;
    this.teamState.setCurrentCard(card);
  }

  switchToTeam() {
    this.mainState.switchToTeam();
  }

  closeCardModal() {
    this.viewCardModal = false;
  }
}