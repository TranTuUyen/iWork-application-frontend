import { Component, Input, OnInit, ChangeDetectorRef } from "@angular/core";
import { CardHttpService } from "@W-App/pages/main/services/card.service";
import { TeamState } from "@W-Base/states";
import { TeamHttpService } from "@W-App/pages/main/services/team.service";

@Component({
  selector: 'board-menu',
  templateUrl: './board.menu.html',
  styleUrls: [
    './board.menu.scss'
  ]
})
export class BoardMenuComp implements OnInit {
  userAvt = 'assets/images/avts/user_avt.png'
  toggleInvite: boolean = false;

  @Input()
  inputToggle: number;
  currentTeam: ITeam;
  activityList: IActivity[];
  activityDetailList: any = [];
  arrtemp: any = [];

  constructor(private cardHttpService: CardHttpService,
    private teamHttpService: TeamHttpService,
    private teamState: TeamState,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.teamState.getCurrentTeam()
      .subscribe(res => {
        this.currentTeam = res;
      })

    this.teamState.getActivityList()
      .subscribe(res => {
        this.activityList = res;
        console.log(this.activityList);
      })

    this.onNewActivity();
  }

  onNewActivity() {
    this.cardHttpService.onNewActivity()
      .subscribe((res: IActivity) => {
        if (res.team == this.currentTeam._id) {
          this.teamState.addNewActivity(res);
          // this.activityList.push(res);
          console.log("New activity");
          console.log(res);
          this.cdr.detectChanges();
        }
      })
  }
}