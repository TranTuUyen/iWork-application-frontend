import {Component, ViewEncapsulation} from '@angular/core'

@Component({
  selector: 'todo-content',
  templateUrl: './todo.content.html',
  styleUrls: [
    './todo.content.scss'
  ],
  encapsulation: ViewEncapsulation.None
})

export class TodoContentComp {
}