import { Component} from '@angular/core'
import { DragulaService } from 'ng2-dragula';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'task',
  templateUrl: './task.html',
  styleUrls: [
    './task.scss'
  ]
})

export class TaskComp {
  constructor(private dragulaService: DragulaService) {
    dragulaService.setOptions('ik-todo-list', {
      moves: function (el: Element, container: Element, handle: Element) {
        console.log(handle.className);
        console.log(handle.className == 'ik-todo-task-drag-handle');
        return handle.className === 'ik-todo-task-drag-handle';
      }
    });
  }

  // ngOnDestroy() {
  //   this.dragulaService.destroy("ik-todo-list");
  // }
}