
import { Component, OnInit} from '@angular/core'
import { TeamHttpService } from '@W-App/pages/main/services/team.service';
import { TeamState } from '@W-Base/states';

@Component({
  selector: 'team-invitation-modal',
  templateUrl: './team.invitation.modal.html',
  styleUrls: [
    './team.invitation.modal.scss'
  ]
})

export class TeamInvitationModalComp implements OnInit{
  teamAvt = 'assets/images/smile_avt.jpg'

  emailToInvite: string;
  currentTeam: ITeam;

  public constructor (private teamHttpService: TeamHttpService, private teamState: TeamState){}

  ngOnInit() {
    this.teamState.getCurrentTeam()
    .subscribe((curTeam: ITeam) => {
      this.currentTeam=  curTeam; 
    })
  }


}