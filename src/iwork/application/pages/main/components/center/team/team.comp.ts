import { Component, OnInit } from '@angular/core'
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { TeamHttpService } from '@W-App/pages/main/services/team.service';
import { TeamState } from '@W-Base/states';
import { JWTService } from '@W-Base/services';

@Component({
  selector: 'team',
  templateUrl: './team.html',
  styleUrls: [
    './team.scss'
  ]
})

@Injectable()
export class TeamComp implements OnInit {
  teamAvt = 'assets/images/avts/team_avt.png'
  user = 'assets/images/avts/user_avt.png'

  togglePermission: boolean = false;
  email: string;

  constructor(private http: Http,
    private teamHttpService: TeamHttpService,
    private teamState: TeamState,
    private jwtService: JWTService) {

  }
  currentTeam: ITeam;
  teamName: String;

  memberArray: any = [];

  userInfo = {} as IUser;
  isAdmin: boolean = false;


  ngOnInit() {

    this.userInfo = JSON.parse(this.jwtService.getUserInfo())
    this.teamState.getCurrentTeam()
      .subscribe((curTeam: ITeam) => {
        this.currentTeam = curTeam;
        if (this.currentTeam.name) {
          this.getMembersInfo();
        }
      })


  }

  getMembersInfo() {
    for (let i = 0; i < this.currentTeam.members.length; i++) {
      if (this.currentTeam.members[i].member == this.userInfo._id && this.currentTeam.members[i].permission == 'Admin') {
        this.isAdmin = true;
      }
      this.teamHttpService.getMemberInfo(this.currentTeam.members[i].member)
        .subscribe(res => {
            let member = res;
            if(this.currentTeam.members[i]) {
              member.permission = this.currentTeam.members[i].permission;
              this.memberArray.push(member);
            }
        })
    }
  }

  changePermission(permission: string, member: IMember) {
    let data = {
      permission: permission
    };
    this.teamHttpService.updateTeamMember(this.currentTeam._id, 'update-member-permission', member._id, data)
      .subscribe(res => {
        console.log(res);
        member.permission = permission;
        if (member._id == this.userInfo._id && permission != 'Admin') {
          this.isAdmin == false;
        }
      })
  }

  removeMember(member: IMember) {
    let i;
    for (i = 0; i < this.memberArray.length; i++) {
      if (this.memberArray[i]._id != member._id && this.memberArray[i].permission == 'Admin') {
        let data = {
          permission: member.permission
        };
        this.teamHttpService.updateTeamMember(this.currentTeam._id, 'remove-member', member._id,data )
          .subscribe(res => {
            const index: number = this.memberArray.map((e: any) => e._id).indexOf(member._id);
            if (index !== -1) {
              this.memberArray.splice(index, 1);
            }
          })
        break;
      }
    }

    if (i == this.memberArray.length) {
      alert("You must set a member is admin first");
    }

  }


  getMemberByEmail(email: string) {
    this.teamHttpService.getMemberByEmail(email)
      .subscribe(member => {
        console.log(member)
        member[0].permission = "Member";
        let data = {
          permission: 'Member'
        };
        this.teamHttpService.updateTeamMember(this.currentTeam._id, "add-member", member[0]._id, data).
          subscribe(res => {
            console.log(res);
            this.memberArray.push(member[0]);
            this.email = '';
          })
      })
  }

  // getMembersInfo1() {
  //   let ids = [];
  //   for(let  i = 0; i < this.currentTeam.members.length; i++) {
  //     ids[i] = this.currentTeam.members[i].member;
  //   }

  //   this.teamHttpService.getMembersInfo(ids)
  //   .subscribe(res => {
  //     console.log(res);
  //     // let member = res;
  //     // member.permission = this.currentTeam.members[i].permission;
  //     // this.memberArray.push(member);
  //   })
  // }

  closeTogglePermission(e: Event) {
    this.togglePermission = false;
  }

  changeTeamName(event: any) {
    this.currentTeam.name = event;
    this.teamHttpService.updateTeam(this.currentTeam)
      .subscribe(res => {
        console.log("Update team successfully!");
      })
  }

  removeTeam() {
    this.teamHttpService.removeTeam(this.currentTeam._id)
      .subscribe(() => {
        // alert("Delete  team successfully!");
        let emptyTeam = { name: '', members: [] } as ITeam;
        let removeTeamId = this.currentTeam._id;
        this.teamState.removeTeam(removeTeamId);
        this.teamState.setCurrentTeam(emptyTeam);
      })
  }
}