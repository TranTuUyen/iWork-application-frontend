import { Component, ViewEncapsulation, OnInit } from '@angular/core'
import { MainState } from '@W-Base/states';
import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { TeamState } from '@W-Base/states/team.state';
import { TeamHttpService } from '@W-App/pages/main/services/team.service';
import { Subject } from 'rxjs';
import { JWTService } from '@W-Base/services';
import { Router } from '@angular/router';

@Component({
  selector: 'left-menu',
  templateUrl: './left.menu.html',
  styleUrls: [
    './../../../../../../../scss/ikicon.scss',
    './left.menu.scss'],
  encapsulation: ViewEncapsulation.None
})

export class LeftMenuComponent implements OnInit {
  toggleMenu: boolean = false;
  private onDestroy$ = new Subject<void>();

  logo = "./assets/theme/img/logo.png";
  userAvt = "./assets/images/avts/user_avt.png";
  teamAvt = "./assets/images/avts/team_avt.png";

  userInfo = {} as IUser;
  teamsArr: ITeam[];

  constructor(private http: Http, private mainState: MainState,
    private teamState: TeamState,
    private teamHttpService: TeamHttpService,
    private jwtService: JWTService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.userInfo = JSON.parse(this.jwtService.getUserInfo())
    this.teamState.getTeamList()
      .subscribe(
        res => {
          this.teamsArr = res;
          console.log("Hello");
          console.log(this.teamsArr);
          if (this.teamsArr[0]) {
            this.teamsArr[0].current = true;
            this.teamState.setCurrentTeam(this.teamsArr[0]);
          }
        },
        err => {
          console.log("Error occured");
        }
      );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  switchToTeam() {
    this.mainState.switchToTeam();
    this.toggleMenu = false;
  }

  chooseTeam = (team: ITeam) => {
    this.teamsArr.forEach(team => team.current = false)
    team.current = true;
    this.teamState.setCurrentTeam(team);
    this.mainState.switchToBoard();
    this.toggleMenu = false;
  }

  logout = () => {
    this.jwtService.removeToken();
    this.jwtService.removeUserInfo();
    this.router.navigate(['/authentication/login']);
  }
}