import {Component} from '@angular/core'

@Component({
  selector: 'user-menu',
  templateUrl: './user.menu.html',
  styleUrls: [
    './../../../../../../../scss/elements.scss',
    './../../../../../../../scss/form.scss',
    './../../../../../../../scss/user.scss',
    './user.menu.scss']
})

export class UserMenuComp {
  userAvt = "./assets/images/avts/user_avt.png";
}