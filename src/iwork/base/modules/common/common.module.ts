import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { TranslateModule } from 'ng2-translate'
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'

@NgModule({
	imports: [
		FormsModule,
		CommonModule,
		TranslateModule,
		Angular2FontawesomeModule,
		
	],
	exports: [
		FormsModule,
		CommonModule,
		TranslateModule,
		Angular2FontawesomeModule
	]
})

export class WCommonModule { }