import {NgModule} from '@angular/core'
import {Http, RequestOptions} from '@angular/http'
import {AuthHttp, AuthConfig} from 'angular2-jwt'
import {JWTAuthVariable} from '@W-Base/variables'
import {JWTService} from '@W-Base/services'

export function authHttpServiceFactory(http: Http, requestOptions: RequestOptions) {
    let config = {
        tokenName: JWTAuthVariable.ACCESS_TOKEN,
        tokenGetter: (() => localStorage.getItem(JWTAuthVariable.ACCESS_TOKEN))
    }

    return new AuthHttp(new AuthConfig(config), http, requestOptions)
}

@NgModule({
    providers: [
        {
            provide: AuthHttp,
            useFactory: authHttpServiceFactory,
            deps: [Http, RequestOptions]
        }
    ]
})

export class JWTAuthModule {}
