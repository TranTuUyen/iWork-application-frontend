export class MemberAPIs {
  // public static GET_MEMBER_LIST = 'http://localhost:3000/user'
  public static GET_MEMBER = 'http://localhost:3000/user/'
  public static GET_MEMBERS = 'http://localhost:3000/users/'
  public static GET_MEMBER_BY_EMAIL = 'http://localhost:3000/user/get-user-by-email/'
}