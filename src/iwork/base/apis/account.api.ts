import Config from '../config/config'

export class AccountAPIs {

    public static ACCOUNT_INIT_PASS     = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/account/init-password`
    public static REGISTER              = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/account/register`
    public static ACCOUNT_INFO          = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/account/current`
    public static CHANGE_PASS           = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/account/change-password`
    public static UPDATE_PROFILE        = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/account/edit-account`

  
}