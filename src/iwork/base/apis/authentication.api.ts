import Config from '../config/config'

export class AuthenticationAPIs {
        public static EMAIL_VERIFICATION_GMAIL = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/gmail/verify-email`
        public static EMAIL_VERIFICATION_OUTLOOK = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/outlook/verify-email`
        public static EMAIL_VERIFICATION = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/account/email-verify`

        public static REFRESH_TOKEN = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/token/refresh`
        public static LOGIN = `${Config.API_BASE_URL}/auth/login`
        public static ACCOUNT_INIT_PASS = `${Config.API_BASE_URL}/api/${Config.AUTHEN_SERVICE_NAME}/account/init-password`
        public static REGISTER = `${Config.API_BASE_URL}/auth/sign-up`
}