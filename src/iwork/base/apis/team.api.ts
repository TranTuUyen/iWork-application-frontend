export class TeamAPIs {
    public static JOINED_TEAMS = 'http://localhost:3000/team/joined'
    public static CREATE_TEAM = 'http://localhost:3000/team/'
    public static GET_CARDS_LIST = 'http://localhost:3000/card/'
    public static ADD_MEMBER = 'http://localhost:3000/teams/add-members/'
    public static REMOVE_MEMBER = 'http://localhost:3000/teams/remove-members/'
    public static ADD_CARD = 'http://localhost:3000/teams/add-card/'
    public static REMOVE_CARD = 'http://localhost:3000/teams/remove-card/'
  }