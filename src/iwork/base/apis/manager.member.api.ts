import Config from '../config/config'

export class ManagerMemberAPIs {
    public static GET_USER_LAB              = `${Config.API_BASE_URL}/api/${Config.LAB_CHANNEL_SERVICE_NAME}/lab/get-lab-members?labId=`
    public static CHANGE_PERMISSION            =`${Config.API_BASE_URL}/api/${Config.LAB_CHANNEL_SERVICE_NAME}/lab/change-permission-member`
    public static GET_LAB_ADMINS            =`${Config.API_BASE_URL}/api/${Config.LAB_CHANNEL_SERVICE_NAME}/lab/get-lab-admins?labId=`
    public static PUT_REMOVE_ADMIN          =`${Config.API_BASE_URL}/api/${Config.LAB_CHANNEL_SERVICE_NAME}/lab/remove-admin`
    public static GET_MEMBER_INVITED        =`${Config.API_BASE_URL}/api/${Config.LAB_CHANNEL_SERVICE_NAME}/lab/get-users-invited-by-lab?labId=`
    public static PUT_DECLINE_INVITATION    =`${Config.API_BASE_URL}/api/${Config.LAB_CHANNEL_SERVICE_NAME}/lab/decline-invitation`
    public static PUT_REMOVE_MEMBER          =`${Config.API_BASE_URL}/api/${Config.LAB_CHANNEL_SERVICE_NAME}/lab/remove-member`
    public static CHECK_LAB_ADMIN       = `${Config.API_BASE_URL}/api/${Config.LAB_CHANNEL_SERVICE_NAME}/lab/check-lab-admin?labId=`
    
    
}