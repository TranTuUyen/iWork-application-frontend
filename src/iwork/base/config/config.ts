export default class Config {
    // public static API_BASE_URL = 'mongodb://127.0.0.1:27017'
    public static API_BASE_URL = 'http://localhost:3000'
    public static AUTHEN_SERVICE_NAME = 'authentication-service'
    public static LAB_CHANNEL_SERVICE_NAME = 'lab-channel-service' 

}