type ICallback = () => void

interface IUser {
    _id?: string,
    name: string,
    email: string,
    imageUrl: string
}
interface IMember {
    _id?: string,
    email: string,
    permission: string,
    member: string
}
interface ICard {
    _id: string,
    team: string,
    content: string,
    startDate: Date,
    dueDate: Date,
    status: string,
    members: any[],
    current?: boolean,
    index: Number
}

interface ITeam {
    _id?: string,
    name: string,
    cards: ICard[],
    members: IMember[],
    admin: IUser,
    profilePicture: string,
    current?: boolean
}

interface IActivity {
    _id: string,
    team: string,
    actor: IUser,
    target: IUser,
    action: string,
    card: ICard,
    timeStamp: Date
}

