import { Injectable } from '@angular/core'

@Injectable()
export class CommonService {
    public static convertArrBuffToJSON(data: ArrayBuffer): any {
        let uint8Array = new Uint8Array(data)
        let decodedString = String.fromCharCode.apply(undefined, uint8Array)
        let jsonObject = JSON.parse(decodedString)

        return jsonObject
    }
   
}
