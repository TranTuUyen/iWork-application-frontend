import { Injectable } from '@angular/core'
import * as io from 'socket.io-client'


import { CommonService, JWTService} from '@W-Base/services'
// import { MessageAPIs } from '@W-Base/apis'
import { EventType } from '@W-Base/enums'

@Injectable()
export class SocketService {
	// public socket: SocketIOClient.Socket;
	public static socket = io('http://localhost:3000');

	// constructor(private jwtService: JWTService, private messageService: MessageService) {
	// 	this.socket = io(MessageAPIs.MESSAGE_LISTENER, {
	// 		forceNew: true,
	// 		query: { token: jwtService.getAccessToken() },
	// 		path: MessageAPIs.SOCKET_PATH
	// 	})
	// }

	// initSocketMethods() {
	// 	this.socket.on('error', (err: any) => {
	// 		console.log(err.message)
	// 		this.socket.close()
	// 	})

	// 	this.socket.on('connect', () => {
	// 		console.log('connected')
	// 	})

	// 	this.socket.on('new-event', (response: ArrayBuffer) => {
	// 		let socketMessage: ISocketMessage = CommonService.convertArrBuffToJSON(response)
	// 		switch (socketMessage.eventType) {
	// 			case EventType.NEW_CHANNEL_MESSAGE:
	// 				// this.messageService.routeChannelMessage(socketMessage.message)
	// 				break
	// 		}
	// 	})
	// }

}