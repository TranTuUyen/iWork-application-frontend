import { Injectable } from '@angular/core'
import { JWTAuthVariable } from '@W-Base/variables'
import { tokenNotExpired, JwtHelper } from 'angular2-jwt'
import { Headers, Http } from '@angular/http'
import { Observable } from 'rxjs/Observable'
import { AuthenticationAPIs } from '@W-Base/apis'


@Injectable()
export class JWTService {
    private jwtHelper: JwtHelper
    constructor(private http: Http) {
        this.jwtHelper = new JwtHelper()
    }

    getAccessToken(): string {
        return localStorage.getItem(JWTAuthVariable.ACCESS_TOKEN)
    }

    addTokens(accessToken: string) {
        localStorage.setItem(JWTAuthVariable.ACCESS_TOKEN, accessToken)
    }

    removeToken() {
        localStorage.removeItem(JWTAuthVariable.ACCESS_TOKEN);
    }

    saveUserInfo(userInfo: any) {
        localStorage.setItem('USER_INFO', JSON.stringify(userInfo))
    }

    removeUserInfo() {
        localStorage.removeItem('USER_INFO');
    }

    getUserInfo(): any {
        return localStorage.getItem('USER_INFO')
    }
}