import { Injectable } from '@angular/core'
import { AuthHttp } from 'angular2-jwt'
import { Observable } from 'rxjs/Observable'
import { RequestOptionsArgs, URLSearchParams, Headers, Http } from '@angular/http'

import { JWTService } from './jwt.service'

@Injectable()
export class AuthHttpService {

	constructor(private authHttp: AuthHttp, private jwtService: JWTService, private http: Http) {
	}

	public get(endPoint: string, queryData?: any): Observable<any> {
		let queryParams
		let optionArgs: RequestOptionsArgs = {}

		if (queryData) {
			queryParams = new URLSearchParams()
			for (let key in queryData) {
				queryParams.set(key, queryData[key])
			}
			optionArgs.search = queryParams
		}
		let myHeader = new Headers();
		myHeader.append('Content-type', 'application/json');
		optionArgs.headers = myHeader;
		return this.authHttp.get(endPoint, optionArgs)
	}

	public post(endPoint: string, data: any): Observable<any> {
		return this.authHttp.post(endPoint, data)
	}

	public put(endPoint: string, data: any): Observable<any> {
		return this.authHttp.put(endPoint, data)
	}

	public delete(endPoint: string, queryData?: any): Observable<any> {
		let queryParams
		let optionArgs: RequestOptionsArgs = {}

		if (queryData) {
			queryParams = new URLSearchParams()
			for (let key in queryData) {
				queryParams.set(key, queryData[key])
			}
			optionArgs.search = queryParams
		}
		let myHeader = new Headers();
		myHeader.append('Content-type', 'application/json');
		optionArgs.headers = myHeader;
		return this.authHttp.delete(endPoint, optionArgs)
	}
}
