import {Injectable} from '@angular/core'
import {AccountService} from '@W-AppPages/account/services/account.service'
import {CommonVariables} from '@W-Base/variables'
import {Observable} from 'rxjs/Observable'

@Injectable()
export class StaySignedInService {

    staySignedIn = false

    constructor(private accountService: AccountService) {}

    setStaySignedIn(remember: boolean) {
        localStorage.setItem(CommonVariables.STAY_SIGNED_IN, remember.toString())
    }

    public isSignedIn(): Observable<any> {
        this.staySignedIn = (localStorage.getItem(CommonVariables.STAY_SIGNED_IN) === 'true')
        if(this.staySignedIn === false) {
            return Observable.throw({isSignedIn: false})
        }

        return this.accountService
            .getAccountProfile()
            .map(response => {
                let result = JSON.parse(response)
                console.log('BBB')

                return {isSignedIn: true}
            })  
            .catch(error => Observable.throw({isSignedIn: false}))
            // .subscribe(
            //     response => {
            //         let result = JSON.parse(response)
            //         console.log('BBB')

            //         return Observable.throw({isSignedIn: true})
            //     },
            //     error => {
            //         return Observable.throw({isSignedIn: false})
            // })
        // console.log('CCC')
    }

}
