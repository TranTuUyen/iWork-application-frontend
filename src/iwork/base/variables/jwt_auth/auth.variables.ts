export class JWTAuthVariable {

    public static TOKEN_HEADER_NAME = 'authorization'
    public static ACCESS_TOKEN = 'access_token'
    public static REFRESH_TOKEN = 'refresh_token'
}