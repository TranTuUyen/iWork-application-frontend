
export class CommonVariables {

    //PARAMs
    public static STAY_SIGNED_IN = 'remember'

    // ERROR CODE
    public static STATUS_CODE_400 = 400
    public static STATUS_CODE_CONNECTION_REFUSE = 0
    public static FIELD_NAME_MAX_LENGTH = 20

    //LAB
    public static DESCRIPTION_MAX_LENGTH = 200;
    public static DESCRIPTION_REQUIRED = 'description_required'
    
    //PROFILE
     

    //CHANGE PASSWORD
    public static CURRENT_PASSWORD_REQUIRED = "alert_required_current_password"
    public static NEW_PASSWORD_REQUIRED  = "alert_required_new_password"
    public static CURRENT_PASSWORD_REQUIRED_LENGTH  = "alert_required_current_password_length"
    public static NEW_PASSWORD_REQUIRED_LENGTH  = "alert_required_new_password_length"         
    public static ALERT_CURRENT_PASSWORD_NOT_MATCH = 'alert_current_password_not_match' 
    public static ALERT_CONFIRM_PASSWORD_NOT_MATCH = 'alert_confirm_password_not_match'     
    // MESSAGE
    public static EMAIL_INVALID = 'authen_register_email_invalid'
    public static EMAIL_WRONG_TYPE = 'authen_register_email_wrong_type'
    public static FIRST_NAME_REQUIRED = 'authen_register_firstname_required'
    public static LAST_NAME_REQUIRED = 'authen_register_lastname_required'
    public static PASSWORD_LENGTH = 'authen_register_password_length'
    public static PASSWORD_REQUIRED = 'authen_register_password_required'
    public static CONFIRM_PASSWORD_NOT_MATCH = 'authen_register_password_not_match'
    public static CURRENT_PASSWORD_NOT_MATCH = 'current_password_not_match'
    public static FIELD_NAME_LENGTH = 'invalid_max_length'

}