import { Injectable } from '@angular/core'
import { Subject, Observable, BehaviorSubject } from 'rxjs'


@Injectable()
export class EmailState { 
 
  constructor(){
  }


  /**
   * 0: not create
   * 1: create new Email
   * 2: reply all Email -> emailsubject
   * 3: repy email -> email
   * 4: forward email -> email
   */
  private  createEmailState = new BehaviorSubject<Number>(0);
  currentcreateEmailState = this.createEmailState.asObservable();
   
  setCreateEmail(state :any){
    this.createEmailState.next(state);
  }
  close(){
    this.createEmailState.next(0);
  }

  // select email subject
  private emailSubjectSelect = new BehaviorSubject<any>([]);
  currentEmailSubjectSelect = this.emailSubjectSelect.asObservable();

  setSelectEmailSubject(emailSubjectSelect: any){
    this.emailSubjectSelect.next(emailSubjectSelect);
  }
  removeEmailSubject(){
    this.emailSubjectSelect.next([]);
  }
 
  // All email
  private allEmailSubject =  new BehaviorSubject<any>([]);
  currentAllEmailSubject = this.allEmailSubject.asObservable();

  setAllEmailSubject(allEmailSubject: any){
    this.allEmailSubject.next(allEmailSubject);
  }

  // all email, with email unread...
  private allEmailSubjectData =  new BehaviorSubject<any>({});
  currentAllEmailSubjectData = this.allEmailSubjectData.asObservable();

  removeEmailSubjectData(){
  this.allEmailSubjectData.next({});  
  }

  setAllEmailSubjectData(allEmailSubjectData: any){
    this.allEmailSubjectData.next(allEmailSubjectData);
  }

  private loadingEmail =  new BehaviorSubject<any>(false);
  currentisLoadingEmail = this.loadingEmail.asObservable();

  isLoadingEmail(){
    this.loadingEmail.next(true);
  }
  endLoadingEmail(){
    this.loadingEmail.next(false);
  }


       // select email subject
  public filterEmailValue = new BehaviorSubject<any>('');
  currentFilterEmailValue = this.filterEmailValue.asObservable(); 
}