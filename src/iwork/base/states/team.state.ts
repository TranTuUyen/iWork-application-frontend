import { Injectable } from '@angular/core'
import { Subject, Observable, BehaviorSubject, ReplaySubject } from 'rxjs'
import { ThreadType } from '@W-Base/enums'
import { MainState } from './main.state'
import { TeamHttpService } from '@W-App/pages/main/services/team.service';
import { CardHttpService } from '@W-App/pages/main/services/card.service';
import { ActivityHttpService } from '@W-App/pages/main/services/activity.service';

@Injectable()
export class TeamState {
	currentTeamId = ''

	teamList: ITeam[]
	teamListSubject: Subject<ITeam[]> = new ReplaySubject<ITeam[]>(1);

	currentTeamSubject: Subject<ITeam> = new ReplaySubject<ITeam>(1);
	currentTeam: ITeam

	cardList: ICard[]
	cardListSubject: Subject<ICard[]>

	currentCardSubject: Subject<ICard> = new ReplaySubject<ICard>(1);
	currentCard: ICard

	activityList: IActivity[] = [];
	activityListSubject: Subject<IActivity[]> = new ReplaySubject<IActivity[]>(1);

	constructor(private mainState: MainState,
		private teamCardHttpService: CardHttpService,
		private teamHttpService: TeamHttpService,
		private activityHttpService: ActivityHttpService) {
		this.cardList = [];
		this.cardListSubject = new Subject<ICard[]>()
	}


	// Team

	public setTeamList = (teams: ITeam[]) => {
		this.teamList = teams;
		this.fetchTeamsList;
		this.teamListSubject.next(this.teamList)
	}

	public getTeamList(): Observable<ITeam[]> {
		this.fetchTeamsList();
		return this.teamListSubject.asObservable()
	}

	public addNewTeam = (team: ITeam) => {
		this.teamList.push(team);
		this.teamListSubject.next(this.teamList);
		
		this.setCurrentTeam(team);
	}

	public addNewActivity = (activity: IActivity) => {
		this.activityList.push(activity);
		this.activityListSubject.next(this.activityList);
	}

	public removeTeam = (teamId: string) => {
		const index: number = this.teamList.map(e => e._id).indexOf(teamId);
		if (index !== -1) {
			this.teamList.splice(index, 1);
			this.teamListSubject.next(this.teamList);
		}
	}

	public setCurrentTeam(team: ITeam) {
		if (this.currentTeam == team && this.mainState.getMainState().type === ThreadType.BOARD) {
			return
		}
		this.teamHttpService.joinTeamRoom(team._id);
		this.currentTeam = team;
		this.currentTeamSubject.next(this.currentTeam);
		if (team.name) {
			this.fetchCardsInTeam();
			this.fetchActivitiesInTeam();
		}
	}

	public getCurrentTeam(): Observable<ITeam> {
		return this.currentTeamSubject.asObservable()
	}

	public fetchTeamsList = () => {
		this.setTeamList([]);
		this.teamHttpService.getJoinedTeams()
			.subscribe(teams => {
				console.log("Team joined: " + JSON.stringify(teams))
				this.setTeamList(teams)
			},
				error => {
				})
	}


	//Card in team
	public setCardList = (cards: ICard[]) => {
		this.cardList = cards
		this.cardListSubject.next(this.cardList)
	}

	public getCardList(): Observable<ICard[]> {
		return this.cardListSubject.asObservable()
	}

	public fetchCardsInTeam = () => {
		this.setCardList([]);
		console.log(this.currentTeam._id);
		this.teamCardHttpService.getCardsByTeam(this.currentTeam._id)
			.subscribe(cards => {
				this.setCardList(cards)
			},
				error => {
				})
	}

	// Activities in team
	public setActivityList = (activities: any[]) => {
		this.activityList = activities
		this.activityListSubject.next(this.activityList)
	}

	public getActivityList(): Observable<any[]> {
		return this.activityListSubject.asObservable()
	}

	public fetchActivitiesInTeam = () => {
		this.setActivityList([]);
		this.activityHttpService.getActivitiesByTeam(this.currentTeam._id)
			.subscribe(activities => {
				this.setActivityList(activities)
				// console.log(activities);
			},
				error => {
				})
	}
	//Card

	public setCurrentCard(card: ICard) {
		if (this.currentCard === card) {
			return
		}

		this.currentCard = card
		console.log(this.currentCard);
		this.currentCardSubject.next(this.currentCard)
	}

	public getCurrentCard(): Observable<ICard> {
		return this.currentCardSubject.asObservable()
	}

}