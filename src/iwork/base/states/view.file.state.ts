import { Injectable } from '@angular/core'
import { Subject, Observable, Subscription } from 'rxjs'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class ViewFileModalState { 

  isShowModal = new BehaviorSubject<any>(false);
  currenShowModal = this.isShowModal.asObservable();

  showModal(){
    this.isShowModal.next(true);
  }

  hideModal(){
    this.isShowModal.next(false);
  }



  dataModal = new BehaviorSubject<any>({}); 
  currendataModal = this.dataModal.asObservable();

  setDataModal(dataModal: any){
    this.dataModal.next(dataModal)
  }

  clearDataModal(){
    this.dataModal.next({})
  }
} 