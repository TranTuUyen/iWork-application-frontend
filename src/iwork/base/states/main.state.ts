import { Injectable } from '@angular/core'
import { Subject, Observable } from 'rxjs'
import { ThreadType } from '@W-Base/enums'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MainState {

  private mainState: IMainState = {} as IMainState
  private mainStateSubject: Subject<IMainState> = new BehaviorSubject<IMainState>(this.mainState)
    
  currentMainState =  this.mainStateSubject.asObservable();
  viewCardModal:boolean =  false;
   
  constructor() { 
    this.mainState = {} as IMainState
  }

  // public setCurrentChannel(channel: IChannel) {
  //   if (this.mainState.type !== ThreadType.CHANNEL || this.mainState.channel._id !== channel._id) {
  //     this.mainState.type = ThreadType.CHANNEL
  //     this.mainState.channel = channel
  //     this.fireNewMainState()
  //   }
  // }

  public switchToTodoList() {
    if (this.mainState.type !== ThreadType.TODO_lIST) {
      this.mainState.type = ThreadType.TODO_lIST
      this.fireNewMainState()
    }
  }

  public switchToTeam() {
    if (this.mainState.type !== ThreadType.TEAM) {
      this.mainState.type = ThreadType.TEAM
      this.fireNewMainState()
    }
  }

  public switchToBoard() {
    if (this.mainState.type !== ThreadType.BOARD) {
      this.mainState.type = ThreadType.BOARD
      this.fireNewMainState()
    }
  }

  fireNewMainState = () => {
    this.mainStateSubject.next(this.mainState)
  }

  public getCurrentMainState(): Observable<IMainState> {
    return this.mainStateSubject.asObservable()
  }
  public getMainState(): IMainState {
    return this.mainState
  }
}