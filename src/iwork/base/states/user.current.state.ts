
import { Injectable } from '@angular/core'
import { Subject, Observable } from 'rxjs'
import { AuthHttpService } from '@W-Base/services'
import { AccountAPIs } from '@W-Base/apis'

@Injectable()
export class CurrentUserState {
  currentUser: IUser

  constructor(private authService: AuthHttpService) {
    this.getCurrentUserInfo()
  }

  getCurrentUserInfo = () => {
    this.authService.get(AccountAPIs.ACCOUNT_INFO)
      .subscribe(response => {
        if (response._body) {
          this.currentUser = JSON.parse(response._body).data
        }
      },
      error => {
        this.currentUser = {} as IUser
      })
  }
}