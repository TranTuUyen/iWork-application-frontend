import { Injectable } from '@angular/core'
import { Subject, Observable, Subscription } from 'rxjs'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class RightScreenState { 
 
  private  rightScreenStateSource = new BehaviorSubject<Number>(3);
  currentRightScreenState = this.rightScreenStateSource.asObservable();
  historyState : any[];
  constructor(){
    this.historyState = [];
  }
  callHistory(){
    if(this.historyState && this.historyState.length>1){
      this.historyState.splice(this.historyState.length-1,1)
      this.rightScreenStateSource.next(this.historyState[this.historyState.length-1]);
    } else{

      }
      this.rightScreenStateSource.next(-1);
  }
  closeAll(rightScreenState: any){
    this.historyState = []; 
    this.rightScreenStateSource.next(-1);
  }
  openScreenState(item: any){
    this.historyState.push(item)
    this.rightScreenStateSource.next(item); 
  }


  // email

  private tabEmailSelect = new  BehaviorSubject<Number>(3);
  currentTabEmailSelect = this.tabEmailSelect.asObservable();

  /**
   * 
   * @param tab 
   * 0: search
   * 1: notify
   * 2: contact
   * 3: email
   * 4: file
   * 5: bookmark
   * 6: view email file
   * 7: profile
   * 8: comment
   */
  changeTab(tab : any){
    this.tabEmailSelect.next(tab);
  }

}