export enum EventType {
  NEW_P2P_MESSAGE= 'new-p2p-message',
  NEW_CHANNEL_MESSAGE= 'new-channel-message',
  EDIT_MESSAGE= 'edit-message',
  DELETE_MESSAGE= 'delete-message',
  TYPING= 'typing',
  REPLY= 'reply',
  REACTION= 'reaction',
  PIN_MESSAGE= 'pin-message',
  UPDATE_READ_TIMESTAMP='update-read-timestamp'
}