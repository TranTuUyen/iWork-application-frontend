export enum ThreadType {
  CHAT = 'CHAT',
  TODO_lIST = 'TODO_LIST',
  TEAM = 'TEAM',
  BOARD = 'BOARD'
}