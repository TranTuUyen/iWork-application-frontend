import {Component} from '@angular/core'

@Component({
    selector: 'loading-page',
    templateUrl: './loading.page.html',
    styleUrls: ['../../../scss/page.scss', '../../../scss/animation.scss', './loading.page.scss']
})

export class LoadingPageComponent{
    logo = 'assets/images/logo_lg.png'
    loadingicon = 'assets/images/circle_loading.gif'
}