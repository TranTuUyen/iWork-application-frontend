import {Component, Input} from '@angular/core'

@Component({
    selector: 'w-tag',
    templateUrl: './w.tag.html',
    styleUrls: [
        '../../../../scss/page.scss'
        ,'../../../../scss/elements.scss'
        ,'./w.tag.scss'
        ,'../../../../scss/helper.scss']
})

export class WTagComponent {
    avt1 = 'assets/images/labs/user_avt_1_md.png'

    @Input() icon: string
    @Input() title: string
}