import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: 'mini-profile',
  templateUrl: 'mini.profile.html',
  styleUrls: [
    '../../../../scss/page.scss'
    , '../../../../scss/elements.scss'
    , './mini.profile.scss'
    , '../../../../scss/helper.scss'
  ]
})

export class MiniProfileComp {

  @Output() viewProfileRightScreen: EventEmitter<string> = new EventEmitter<string>();

  @Input() showProfile: any;

  user = 'assets/images/smile_avt.jpg'
  coverimg = 'assets/images/sunset.jpg'
  defaultProfilePicture = 'assets/images/chat/avt1_61x61.png'

  viewProfile(userId: string) {
    this.viewProfileRightScreen.emit(userId);
  }

}