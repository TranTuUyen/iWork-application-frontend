import { Component } from '@angular/core'
import { LoginService } from './login.service'
import { Router } from '@angular/router'
import { AuthenVariables } from '../authen.variables'
import { AuthenMessages } from '../authen.messages'
import { StaySignedInService, JWTService } from '@W-Base/services'

declare const gapi: any;

@Component({
	selector: 'login',
	templateUrl: './login.html',
	styleUrls: ['../../../../../scss/page.scss',
		'../../../../../scss/section.scss',
		'../../../../../scss/form.scss',
		'../../../../../scss/button.scss',
		'../../../../../scss/elements.scss',
		'../../../home_config/home.scss',
		'../authentication.scss',
		'./login.scss',
		'../../../../../scss/helper.scss']
})

export class LoginComponent {
	hasError: boolean = false
	errorMsg: string = ''
	userInfo = {
		email: '',
		password: ''
	}
	// remember: boolean = false



	constructor(private loginService: LoginService,
		private jwtService: JWTService,
		private singedInService: StaySignedInService,
		private router: Router) { }

	// rememberMe() {
	// 	this.remember = !this.remember
	// }

	public onLogin() {
		this.hasError = false
		this.loginService
			.login(this.userInfo)
			.subscribe(
			response => {
				let tokenData = JSON.parse(response._body)
				this.jwtService.addTokens(tokenData.token)
				this.jwtService.saveUserInfo(tokenData.user)
				this.router.navigate(['/application'])
			},
			error => {
				this.errorMsg = AuthenMessages.LOGIN_ERROR
				if (error.status === AuthenVariables.STATUS_CODE_400) {
					let errorData = JSON.parse(error._body)
					switch (errorData.message) {
						case AuthenVariables.AUTHEN_BACKEND_MESSAGES.ACCOUNT_INACTIVE:
							this.errorMsg = AuthenMessages.LOGIN_ACCOUNT_INACTIVE
							break
						default:
							this.errorMsg = AuthenMessages.LOGIN_INVALID
					}
				}
				this.hasError = true
			}
			)
	}

	

// 	public auth2: any;
// 	public googleInit() {
// 		gapi.load('auth2', () => {
// 			this.auth2 = gapi.auth2.init({
// 				client_id: '298952384630-s3te724tfkjt25cpgjqp5nt9ihf5kt39.apps.googleusercontent.com',
// 				cookiepolicy: 'single_host_origin',
// 				scope: 'profile email'
// 			});
// 			this.attachSignin(document.getElementById('googleBtn'));
// 		});
// 	}
// 	public attachSignin(element: any) {
// 		this.auth2.attachClickHandler(element, {},
// 			(googleUser: any) => {

// 				let profile = googleUser.getBasicProfile();
// 				console.log('Token || ' + googleUser.getAuthResponse().id_token);
// 				console.log('ID: ' + profile.getId());
// 				console.log('Name: ' + profile.getName());
// 				console.log('Image URL: ' + profile.getImageUrl());
// 				console.log('Email: ' + profile.getEmail());
// 				//YOUR CODE HERE


// 			}, (error: any) => {
// 				alert(JSON.stringify(error, undefined, 2));
// 			});
// 	}

// 	ngAfterViewInit() {
// 		this.googleInit();
// 	}
}