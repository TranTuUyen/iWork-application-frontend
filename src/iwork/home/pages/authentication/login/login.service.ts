import {Injectable} from '@angular/core'
import {Http} from '@angular/http'
import {Observable} from 'rxjs/Observable'
import {AuthenticationAPIs} from '@W-Base/apis'

@Injectable()
export class LoginService {
    constructor(private http: Http) {}

    public login(userInfo: any): Observable<any> {
        return this.http.post(AuthenticationAPIs.LOGIN, userInfo)
                        .map(response => response)
                        .catch(error => Observable.throw(error))
    }
}