import {NgModule} from '@angular/core'
import {AuthenRouting} from './authen.routing'
import {ReactiveFormsModule} from '@angular/forms'
import {WCommonModule} from '@W-Base/modules'

import {InitPasswordService} from './register/init_password/init.password.service'
import {EmailVerificationService} from './register/email_verification/email.verification.service'
import {EmailVerificationRedirectService} from './register/email_verification_redirect/email.verification.redirect.service'
import {RegisterService} from './register/register.service'
import {LoginService} from './login/login.service'
import {EmailValidatorService, JWTService} from '@W-Base/services'

import {EmailVerificationComponent} from './register/email_verification/email.verification.component'
import {EmailVerificationRedirectComponent} from './register/email_verification_redirect/email.verification.redirect.component'
import {LoginComponent} from './login/login.component'
import {RegisterComponent} from './register/register.component'
import {InitPasswordComponent} from './register/init_password/init.password.component'
import {TranslateModule} from 'ng2-translate'
import {Confirm} from './register/confirm/confim.comp';

@NgModule({
  imports: [
    WCommonModule,
    AuthenRouting,
    TranslateModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    InitPasswordComponent,
    EmailVerificationComponent,
    EmailVerificationRedirectComponent,
    Confirm
  ],
  providers: [
    InitPasswordService,
    RegisterService,
    EmailVerificationService,
    EmailVerificationRedirectService,
    LoginService,
    JWTService,
    EmailValidatorService
  ]
})

export class AuthenModule {
}
