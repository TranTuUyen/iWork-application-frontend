import {Http, Response, Headers, URLSearchParams, RequestOptions} from '@angular/http'
import {Injectable} from '@angular/core'
import {Observable} from 'rxjs/Rx'
import {AuthenticationAPIs} from '@W-Base/apis'

@Injectable()
export class EmailVerificationService {
    constructor(private http: Http) {}

    public emailVerify(token: string): Observable<any> {
    	let params = new URLSearchParams()
        params.set('token', token)
        let requestOption = new RequestOptions()
        requestOption.params = params
        
        return this.http.get(AuthenticationAPIs.EMAIL_VERIFICATION, requestOption)// params
                        .map(response => response)
                        .catch(error => Observable.throw(error || 'Server error'))
    }

}