import {Component, AfterViewInit} from '@angular/core'
import {EmailVerificationService} from './email.verification.service'
import {Router, ActivatedRoute, ParamMap} from '@angular/router'

@Component({
    selector: 'email-verification',
    templateUrl: './email.verification.html'
})

export class EmailVerificationComponent{

    constructor(private emailVerificationService: EmailVerificationService,
                private router: Router,
                private activatedRoute: ActivatedRoute) {}

    public ngAfterViewInit() {
        let verificationToken
        this.activatedRoute
            .queryParamMap
            .switchMap((params: ParamMap) => params.getAll('token'))
            .subscribe(token => verificationToken = token)
        this.emailVerificationService
            .emailVerify(verificationToken) //token param
            .subscribe(
                response => {
                    window.location.href = response._body
                },
                error => {
                    //TODO: Show popup here
                    alert('Error occur, Please try again!')
                    console.log(error)
                }
            )
    }
}