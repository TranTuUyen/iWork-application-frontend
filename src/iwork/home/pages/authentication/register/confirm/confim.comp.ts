import {Component} from '@angular/core'

@Component({
  selector: 'confirm',
  templateUrl: './confirm.html',
  styleUrls: [
    './../../../../../../scss/page.scss',
    './../../../../../../scss/section.scss',
    './../../../../../../scss/form.scss',
    './../../../../../../scss/button.scss',
    './../../../../../../scss/elements.scss',
    './../../../../home_config/home.scss',
    './../../authentication.scss',
    './../../../../../../scss/helper.scss']
})

export class Confirm {}