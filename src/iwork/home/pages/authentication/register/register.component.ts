import {Component} from '@angular/core'
import {RegisterService} from './register.service'
import {TranslateService} from 'ng2-translate'
import {AuthenVariables} from '../authen.variables'
import {EmailValidatorService, JWTService} from '@W-Base/services'
import { Router } from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.html',
  styleUrls: ['../../../../../scss/page.scss',
    '../../../../../scss/section.scss',
    '../../../../../scss/form.scss',
    '../../../../../scss/button.scss',
    '../../../../../scss/elements.scss',
    '../../../home_config/home.scss',
    './register.scss',
    '../authentication.scss',
    '../../../../../scss/helper.scss']
})

export class RegisterComponent {
  accountData = {
    email: '',
    password: ''
  }
  confirmPassword: ''

  fieldError = {
    email: false,
    password: false,
    confirmPassword: false
  }

  errorMessage = {
    email: '',
    password: '',
    confirmPassword: ''
  }

  constructor(private registerService: RegisterService,
              private emailValidator: EmailValidatorService,
            private jwtService: JWTService,
            private router: Router) {
  }

  validateEmail() {
    this.fieldError.email = false
    if (!this.emailValidator.validateEmailFormat(this.accountData.email)) {
      this.fieldError.email = true
      this.errorMessage.email = AuthenVariables.EMAIL_INVALID

      return
    }
    if (!this.emailValidator.validateEmailType(this.accountData.email)) {
      this.fieldError.email = true
      this.errorMessage.email = AuthenVariables.EMAIL_WRONG_TYPE

      return
    }
  }

  // validateFirstName() {
  //   this.fieldError.firstname = false
  //   if (this.accountData.userInfo.firstname === '') {
  //     this.fieldError.firstname = true
  //     this.errorMessage.firstname = AuthenVariables.FIRST_NAME_REQUIRED

  //     return
  //   }
  // }

  // validateLastName() {
  //   this.fieldError.lastname = false
  //   if (this.accountData.userInfo.lastname === '') {
  //     this.fieldError.lastname = true
  //     this.errorMessage.lastname = AuthenVariables.LAST_NAME_REQUIRED

  //     return
  //   }
  // }

  validatePassword() {
    this.fieldError.password = false
    if (this.accountData.password.length === 0) {
      this.fieldError.password = true
      this.errorMessage.password = AuthenVariables.PASSWORD_REQUIRED

      return
    }
    if (this.accountData.password.length < 8 || this.accountData.password.length > 30) {
      this.fieldError.password = true
      this.errorMessage.password = AuthenVariables.PASSWORD_LENGTH

      return
    }
  }

  validateConfirmPassword() {
    this.fieldError.confirmPassword = false
    if (this.confirmPassword.length > 0 && this.confirmPassword !== this.accountData.password) {
      this.fieldError.confirmPassword = true
      this.errorMessage.confirmPassword = AuthenVariables.CONFIRM_PASSWORD_NOT_MATCH

      return
    }
  }

  onKeyPress(field: any) {
    this.fieldError[field] = false
  }

  onRegister() {
    this.validateEmail()
    // this.validateFirstName()
    // this.validateLastName()
    this.validatePassword()
    this.validateConfirmPassword()
    for (let field in this.fieldError) {
      if (this.fieldError[field] === true) {
        return
      }
    }
    this.register()
  }

  private register() {
    this.registerService
      .registerPost(JSON.stringify(this.accountData))
      .subscribe(
        response => {
          console.log(response)
          // console.log(response._body);
          let tokenData = JSON.parse(response._body)
          this.jwtService.addTokens(tokenData.token)
          this.jwtService.saveUserInfo(tokenData.user)
          this.router.navigate(['/application'])
          // alert(response.message)
        },
        error => {

          //TODO: need to implement
          if (error.status === AuthenVariables.STATUS_CODE_CONNECTION_REFUSE) {
            alert('Error occur. Please try again!')

            return
          }
          // TODO: re-implement BE and FE to show message appropriate language
          let errorResponse = JSON.parse(error._body)
          alert(errorResponse.message)
        }
      )
  }

}
