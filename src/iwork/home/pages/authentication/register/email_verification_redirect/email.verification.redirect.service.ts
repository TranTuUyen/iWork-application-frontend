import {Http, Response, Headers, URLSearchParams, RequestOptions} from '@angular/http'
import {Injectable} from '@angular/core'
import {Observable} from 'rxjs/Rx'
import {AuthenticationAPIs} from '@W-Base/apis'

@Injectable()
export class EmailVerificationRedirectService {
    constructor(private http: Http) {}

    public emailVerifyRedirect(emailType: string, emailCode: string): Observable<any> {
        let submitEmailCodeUrl
        switch (emailType) {
            case 'gmail':
                submitEmailCodeUrl = AuthenticationAPIs.EMAIL_VERIFICATION_GMAIL
                break
            case 'outlook':
                submitEmailCodeUrl = AuthenticationAPIs.EMAIL_VERIFICATION_OUTLOOK
                break
            default:
            	throw Error()
        }

        let params = new URLSearchParams()
        params.set('code', emailCode)
        let requestOption = new RequestOptions()
        requestOption.params = params

        return this.http.get(submitEmailCodeUrl, requestOption)
                    .map(response => response)
                    .catch(error => Observable.throw(error || 'Server error'))
    }
}