import {Component, AfterViewInit} from '@angular/core'
import {EmailVerificationRedirectService} from './email.verification.redirect.service'
import {Router, ActivatedRoute, ParamMap} from '@angular/router'
@Component({
    selector: 'email-redirect-verification',
    templateUrl: './email.verification.redirect.html'
})

export class EmailVerificationRedirectComponent{

    constructor(private emailVerificationRedirectService: EmailVerificationRedirectService,
                private router: Router,
                private activatedRoute: ActivatedRoute) {}
    public ngAfterViewInit() {
        let emailType
        let emailCode
        this.activatedRoute
            .paramMap
            .switchMap((params: ParamMap) => params.getAll('emailType'))
            .subscribe(type => emailType = type)
        this.activatedRoute
            .queryParamMap
            .switchMap((params: ParamMap) => params.getAll('code'))
            .subscribe(code => emailCode = code)
        this.emailVerificationRedirectService
            .emailVerifyRedirect(emailType, emailCode)
            .subscribe(
                response => {
                    alert(response)
                },
                error => {
                    //TODO: Show popup here
                    alert('Error occur, Please try again!')
                    console.log(error)
                }
            )
    }
}