import {Component, OnInit} from '@angular/core'
import {InitPasswordService} from './init.password.service'
import {AuthenVariables} from '../../authen.variables'
import {EmailValidatorService} from '@W-Base/services'

import {Router} from '@angular/router'

@Component({
    selector: 'init-password',
    templateUrl: './init.password.html',
    styleUrls: [ '../../../../../../scss/page.scss',
      '../../../../../../scss/section.scss',
      '../../../../../../scss/form.scss',
       '../../../../../../scss/button.scss',
      '../../../../home_config/home.scss',
        '../../authentication.scss',
      './init.password.scss',
      '../../../../../../scss/helper.scss']
})

export class InitPasswordComponent implements OnInit{
    accountData = {
        email: '',
        password: '',
        userInfo: {
            firstname: '',
            lastname: ''
        }
    }
    confirmPassword: string = ''

    fieldError = {
        email: false,
        firstname: false,
        lastname: false,
        password: false,
        confirmPassword: false
    }

    errorMessage = {
        email: '',
        firstname: '',
        lastname: '',
        password: '',
        confirmPassword: ''
    }

    constructor(private initPasswordService: InitPasswordService,
                private router: Router,
                private emailValidator: EmailValidatorService) {}

    ngOnInit() {
        let userData = this.initPasswordService.userData
        this.accountData.email = userData.account.email
        this.accountData.userInfo.firstname = userData.account.userInfo.firstname
        this.accountData.userInfo.lastname = userData.account.userInfo.lastname
    }

    validateEmail() {
        this.fieldError.email = false
        if (!this.emailValidator.validateEmailFormat(this.accountData.email)) {
            this.fieldError.email = true
            this.errorMessage.email = AuthenVariables.EMAIL_INVALID

            return
        }
        if (!this.emailValidator.validateEmailType(this.accountData.email)) {
            this.fieldError.email = true
            this.errorMessage.email = AuthenVariables.EMAIL_WRONG_TYPE

            return
        }
    }

    validateFirstName() {
        this.fieldError.firstname = false
        if (this.accountData.userInfo.firstname === '') {
            this.fieldError.firstname = true
            this.errorMessage.firstname = AuthenVariables.FIRST_NAME_REQUIRED

            return
        }
    }

    validateLastName() {
        this.fieldError.lastname = false
        if (this.accountData.userInfo.lastname === '') {
            this.fieldError.lastname = true
            this.errorMessage.lastname = AuthenVariables.LAST_NAME_REQUIRED

            return
        }
    }

    validatePassword() {
        this.fieldError.password = false
        if (this.accountData.password.length === 0) {
            this.fieldError.password = true
            this.errorMessage.password = AuthenVariables.PASSWORD_REQUIRED

            return
        }
        if (this.accountData.password.length < 8 || this.accountData.password.length > 30) {
            this.fieldError.password = true
            this.errorMessage.password = AuthenVariables.PASSWORD_LENGTH

            return
        }
    }

    validateConfirmPassword(checkEmpty: boolean) {
        this.fieldError.confirmPassword = false
        if (((this.confirmPassword.length > 0 && checkEmpty === false) || (this.confirmPassword.length === 0 && checkEmpty === true))
            && this.confirmPassword !== this.accountData.password) {
            this.fieldError.confirmPassword = true
            this.errorMessage.confirmPassword = AuthenVariables.CONFIRM_PASSWORD_NOT_MATCH

            return
        }
    }

    onKeyPress(field: any) {
        this.fieldError[field] = false
    }

    onInitPassword() {
        this.validateEmail()
        this.validateFirstName()
        this.validateLastName()
        this.validatePassword()
        this.validateConfirmPassword(true)
        for(let field in this.fieldError) {
            if (this.fieldError[field] === true) {
                return
            }
        }
        this.initPassword()
    }

    private initPassword() {
        this.initPasswordService
            .initPassword(this.accountData)
            .subscribe(
                response => {
                    console.log(response)
                    if (response.ok === true) {
                        this.router.navigate(['/'])
                    } else {
                        alert('Error occur, Please try again!')
                        console.log(response)
                    }
                },
                error => {
                    //TODO: need to implement
                    if (error.status === AuthenVariables.STATUS_CODE_CONNECTION_REFUSE) {
                        alert('Error occur. Please try again!')
                        
                        return
                    }
                    // TODO: re-implement BE and FE to show message appropriate language
                    let errorResponse = JSON.parse(error._body)
                    alert(errorResponse.message)
                }
            )
    }
}