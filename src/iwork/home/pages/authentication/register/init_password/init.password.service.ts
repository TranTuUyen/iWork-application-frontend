import {Http, Headers} from '@angular/http'
import {Injectable} from '@angular/core'
import {Observable} from 'rxjs/Rx'
import {AuthHttpService} from '@W-Base/services'
import {AuthenticationAPIs} from '@W-Base/apis'

@Injectable()
export class InitPasswordService {
    public userData: any
    constructor(private http: Http, private authHttpService: AuthHttpService) {}

    public initPassword(accountData: any): Observable<any> {
        return this.authHttpService.put(AuthenticationAPIs.ACCOUNT_INIT_PASS, accountData)
                        .map(response => response)
                        .catch(error => Observable.throw(error || 'Server error'))
    }

}