import { Injectable } from '@angular/core'
import { Http, Response, Headers } from '@angular/http'
import { Observable } from 'rxjs/Rx'
import { AuthenticationAPIs } from '@IWork/base/apis'
@Injectable()
export class RegisterService {

    constructor(private http: Http) { }

    public registerPost(accountData: any): Observable<any> {
        let headers = new Headers({'Content-Type': 'application/json'});
       
        return this.http.post(AuthenticationAPIs.REGISTER, accountData, { headers })
            .map(response => response)
            .catch(error => Observable.throw(error || 'Server error'))
    }
}

