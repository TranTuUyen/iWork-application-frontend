import {CommonVariables} from '@W-Base/variables'

export class AuthenMessages extends CommonVariables {
    
    public static LOGIN_INVALID = 'authen_login_invalid'
    public static LOGIN_ERROR = 'authen_login_error'
    public static LOGIN_ACCOUNT_INACTIVE = 'authen_login_account_inactive'
     
}