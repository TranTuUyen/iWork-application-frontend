import {AuthenMessages} from './authen.messages'
import {AuthenModule} from './authen.module'
export class AuthenVariables extends AuthenMessages {

    

    public static AUTHEN_PARAMS = {
        PARAM_EMAIL_TYPE: 'emailType',
        PARAM_CODE: 'code',
        IS_INIT_PASS: 'yes'
    }

    public static AUTHEN_ROUTES = {
        ROUTE_INIT_PASS: '/authentication/init-password'
    }

    public static AUTHEN_BACKEND_MESSAGES = {
        ACCOUNT_INACTIVE: 'Account is inactive'
    }
    
}