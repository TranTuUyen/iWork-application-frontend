import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'
import {LoginComponent} from './login/login.component'
import {RegisterComponent} from './register/register.component'
import {InitPasswordComponent} from './register/init_password/init.password.component'
import {AuthenVariables} from './authen.variables'
import {EmailVerificationComponent} from './register/email_verification/email.verification.component'
import {EmailVerificationRedirectComponent} from './register/email_verification_redirect/email.verification.redirect.component'
const authenRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'init-password',
        component: InitPasswordComponent
    },
    {
        path: `email-verification`,
        component: EmailVerificationComponent
    },
    {
        path: `email-verification-redirect/:emailType`,
        component: EmailVerificationRedirectComponent
    }
]

@NgModule({
    imports: [RouterModule.forChild(authenRoutes)],
    exports: [RouterModule]
})

export class AuthenRouting {
}