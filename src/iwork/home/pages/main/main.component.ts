import {Component} from '@angular/core'

@Component({
    selector: 'main',
    templateUrl: './main.html',
  styleUrls: [
    './../../../../scss/page.scss',
    './../../../../scss/helper.scss'
  ]
})

export class MainComponent {
    appInfo = {
                name : 'IWork Application',
                version: '1.0',
                image: 'assets/images/test.jpg'
            }
}