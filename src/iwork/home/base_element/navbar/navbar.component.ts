import {Component} from '@angular/core'
import {StaySignedInService} from '@W-Base/services'

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.html',
    styleUrls: [ '../../../../scss/page.scss', './navbar.scss']
})

export class NavBarComponent {
    appLogo = './../../../../assets/images/home/logo.png'
    constructor(private signedInService: StaySignedInService) {
        console.log(signedInService.staySignedIn)
    }
}