import { Component } from '@angular/core'

@Component({
    selector: 'app-footer',
    templateUrl: './footer.html',
    styleUrls: ['../../../../scss/page.scss', './footer.scss', '../../../../scss/helper.scss']
})

export class FooterComponent {
    logo = 'assets/images/home/logo.png'
}