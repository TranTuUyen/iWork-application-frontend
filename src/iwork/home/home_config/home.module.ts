import {NgModule} from '@angular/core'
import {HomeRouting} from './home.routing'
import {TranslateModule} from 'ng2-translate'

import {HomeComponent} from './home.component'
import {MainComponent} from '@W-Home/pages/main/main.component'
import {NavBarComponent} from '@W-Home/base_element/navbar/navbar.component'
import {FooterComponent} from '@W-Home/base_element/footer/footer.component'

@NgModule({
    imports: [
        HomeRouting,
        TranslateModule
    ],
    declarations: [
        MainComponent,
        NavBarComponent,
        FooterComponent,
        HomeComponent
    ],
    providers: []
})

export class HomeModule {}