import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'

import {HomeComponent} from './home.component'
import {MainComponent} from '@W-Home/pages/main/main.component'

const homeRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                component: MainComponent
            },
            {
                path: 'authentication',
                loadChildren: () => new Promise(resolve => {
                    (require as any).ensure([], (require: any) => {
                        resolve(require('../pages/authentication/authen.module').AuthenModule)
                    })
                })
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
})

export class HomeRouting {}

