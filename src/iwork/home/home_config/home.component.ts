import {Component, ViewEncapsulation} from '@angular/core'

@Component({
    selector: 'home',
    templateUrl: './home.html',
    styleUrls: [
      './../../../scss/page.scss',
      './../../../scss/helper.scss',
      './home.scss',
    ],
  encapsulation: ViewEncapsulation.None

})

export class HomeComponent {}