import { Component, OnInit } from '@angular/core'
import { TranslateService } from 'ng2-translate'

@Component({
	selector: 'iwork-app',
	templateUrl: './iwork.html',
	styleUrls: ['../../scss/page.scss', './iwork.scss']
})

export class iworkComponent implements OnInit {

	constructor(private translate: TranslateService) {
		translate.addLangs(['en', 'vi'])
		translate.setDefaultLang('en')
	}

	ngOnInit(){
		console.log('hello world');
	}
} 