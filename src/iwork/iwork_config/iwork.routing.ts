import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'

const iworkRoutes: Routes = [
    {
        path: '',
        loadChildren: () => new Promise(resolve => {
            (require as any).ensure([], (require: any) => {
                resolve(require('../home/home_config/home.module').HomeModule)
            })
        })
    },
    {
        path: 'application',
        loadChildren: () => new Promise(resolve => {
            (require as any).ensure([], (require: any) => {
                resolve(require('../application/application_config/application.module').ApplicationModule)
            })
        })
    },
    {
        path: '**',
        redirectTo: '/'
    }
]

@NgModule({
    imports: [RouterModule.forRoot(iworkRoutes)],
    exports: [RouterModule]
})

export class iworkRouting {}