import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HttpModule } from '@angular/http'
import { Router } from '@angular/router'
import { JWTAuthModule } from '@W-Base/modules'
import { TranslateModule } from 'ng2-translate'
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations'

import { iworkComponent } from './iwork.component'
import { iworkRouting } from './iwork.routing'
import { AuthHttpService, JWTService, StaySignedInService, SocketService } from '@W-Base/services'
import { AccountService } from '@W-AppPages/account/services/account.service'
import { WCommonModule } from '@W-Base/modules'
import { ToasterModule } from '@W-App/plugin/toaster/toaster.module';  
import { CardHttpService } from '@W-App/pages/main/services/card.service';
import { TeamHttpService } from '@W-App/pages/main/services/team.service';
import { ActivityHttpService } from '@W-App/pages/main/services/activity.service';

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpModule,
		iworkRouting,
		JWTAuthModule,
		TranslateModule.forRoot(),
		WCommonModule,
		BrowserAnimationsModule,
		NoopAnimationsModule,
		ToasterModule
	],
	providers: [
		AuthHttpService,
		JWTService,
		StaySignedInService,
		AccountService,
		HttpModule,
		CardHttpService,
		TeamHttpService,
		ActivityHttpService,
		SocketService
	],
	declarations: [
		iworkComponent 
	],
	bootstrap: [iworkComponent]
})
export class iworkModule { }