import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'
import {iworkModule} from './iwork/iwork_config/iwork.module'
import {enableProdMode} from '@angular/core'

enableProdMode()
platformBrowserDynamic().bootstrapModule(iworkModule)