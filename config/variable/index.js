const helper = require('../helper');

const urls = {
  image: helper.url('assets', 'image'),
  font: helper.url('assets', 'font'),
  style: helper.url('assets', 'style')
};

module.exports = {
  urls
};