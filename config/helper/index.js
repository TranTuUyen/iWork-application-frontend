const pathToUrl = (filePath) => {
  return (filePath || '').replace(/\\/g, '/');
};

const url = (...args) => {
  return pathToUrl(args.join('/'));
};

module.exports = {
  url,
  pathToUrl,
  root
};