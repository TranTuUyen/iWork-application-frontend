var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

const path = require('path');
const urls = require('../variable');
const { TsConfigPathsPlugin } = require('awesome-typescript-loader');

module.exports = {
    entry: {
        polyfills: './src/polyfills.ts',
        vendor: './src/vendor.ts',
        app: './src/main.ts'
    },
    output: {
        path: path.join(__dirname, '../../dist'),
        filename: '[name].js',
        chunkFilename: '[name].chunk.js',
        publicPath: ''
    },
    resolve: {
        extensions: ['*', '.js', '.ts'],
        modules: [path.join(__dirname, 'src'), 'node_modules'],
        plugins: [
          new TsConfigPathsPlugin(/* { tsconfig, compiler } */)
        ]
    },
    module: {
        loaders: [{
                    test: /\.ts$/,
                    loaders: ['awesome-typescript-loader', 'angular2-template-loader']
                }, {
                    test: /\.html$/,
                    loader: 'html-loader',
                    options: {
                        minimize: true,
                        removeComments: true,
                        collapseWhitespace: true,

                        // angular 2 templates break if these are omitted
                        removeAttributeQuotes: false,
                        keepClosingSlash: true,
                        caseSensitive: true,
                        conservativeCollapse: true,
                    }
                }, {
                    test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                    loader: 'url-loader'
                }, {
                    test: /\.css$/,
                    loaders: ['to-string-loader', 'css-loader']
                }, {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    loaders: ['raw-loader', 'sass-loader']
                }, { 
                    test: /bootstrap\/dist\/js\/umd\//,
                    loader: 'imports?jQuery=jquery' 
                }
            ]},
        plugins: [
            new webpack.optimize.CommonsChunkPlugin({
                name: ['app', 'vendor', 'polyfills']
            }),
            new HtmlWebpackPlugin({
                template: 'src/index.html'
            }),
            new CopyWebpackPlugin([{
                from: 'src/assets',
                to:  'assets'
            }, {
                from: 'src/i18n',
                to:  'i18n'
            }
            ]),
             new webpack.ProvidePlugin({
                jQuery: 'jquery',
                $: 'jquery',
                jquery: 'jquery'
            })
        ]
};